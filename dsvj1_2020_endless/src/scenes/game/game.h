#ifndef GAME_H
#define GAME_H

#include "scenes/menu/menu.h"
#include "scenes/instructions/instructions.h"
#include "scenes/instructions/instructions2.h"
#include "scenes/gameplay/gameplay.h"
#include "scenes/settings/settings.h"
#include "scenes/credits/credits.h"
#include "scenes/credits/credits2.h"
#include "scenes/result/result.h"
#include "scenes/game_audio/game_audio.h"
#include "scenes/resolutions/resolutions.h"
#include "functions_library/positions/positions.h"

namespace endless
{
	namespace game
	{
		enum class Scene { MENU, INSTRUCTIONS, INSTRUCTIONS2, SETTINGS, CREDITS, CREDITS2, GAMEPLAY, RESULT, AUDIO, RESOLUTIONS };

		extern Scene currentScene;
		extern Scene previousScene;

		extern bool exitButton;

		static void init();
		static void update();
		static void draw();
		static void deinit();

		void run();
	}
}

#endif