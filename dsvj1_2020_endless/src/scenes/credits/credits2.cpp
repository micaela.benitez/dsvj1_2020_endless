#include "credits2.h"

using namespace endless;
using namespace gameplay;

namespace endless
{
	namespace credits2
	{
		static Color leftArrowButtonStatus = WHITE;
		static Color menuButtonStatus = WHITE;

		void init()
		{
			textures::leftArrow = LoadTexture("res/raw/textures/leftarrow.png");
			textures::leftArrow.width = sizes::arrowsSize;
			textures::leftArrow.height = sizes::arrowsSize;
		}

		void update()
		{
			mousePoint = GetMousePosition();

			if (CheckCollisionPointRec(mousePoint, { pos::upButtons, pos::upButtons, (float)textures::rightArrow.width, (float)textures::rightArrow.height }))
			{
				leftArrowButtonStatus = DARKGRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					game::currentScene = game::Scene::CREDITS;
				}
			}
			else leftArrowButtonStatus = WHITE;

			if (CheckCollisionPointRec(mousePoint, { pos::credits2UpButton, pos::upButtons, (float)textures::menuButton2.width, (float)textures::menuButton2.height }))
			{
				menuButtonStatus = DARKGRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					game::currentScene = game::Scene::MENU;
				}
			}
			else menuButtonStatus = WHITE;

			backgroundMotion();

			audio::menuAudio();
			sounds.button = false;
		}

		void draw()
		{
			drawingBackground();

			DrawText("Credits", (screenWidth / 2) - MeasureText("Credits", sizes::textSize5 / 2), pos::creditsTitlePosY, sizes::textSize5, WHITE);
			DrawText("Space runner V 2.0", (screenWidth / 2) - MeasureText("Space runner V 0.1", sizes::textSize / 2), pos::versionPosY, sizes::textSize, WHITE);

			DrawText("Audio", (screenWidth / 2) - MeasureText("Audio", sizes::textSize2 / 2), pos::credits2PosY, sizes::textSize2, WHITE);
			DrawText("lena_orsa from freesound.org", (screenWidth / 2) - MeasureText("lena_orsa from freesound.org", sizes::textSize / 2), pos::credits2PosY1, sizes::textSize, LIGHTGRAY);
			DrawText("Drakensson from freesound.org", (screenWidth / 2) - MeasureText("Drakensson from freesound.org", sizes::textSize / 2), pos::credits2PosY2, sizes::textSize, LIGHTGRAY);
			DrawText("V-ktor from freesound.org", (screenWidth / 2) - MeasureText("V-ktor from freesound.org", sizes::textSize / 2), pos::credits2PosY3, sizes::textSize, LIGHTGRAY);
			DrawText("bolkmar from freesound.org", (screenWidth / 2) - MeasureText("bolkmar from freesound.org", sizes::textSize / 2), pos::credits2PosY4, sizes::textSize, LIGHTGRAY);
			DrawText("uEffects from freesound.org", (screenWidth / 2) - MeasureText("uEffects from freesound.org", sizes::textSize / 2), pos::credits2PosY5, sizes::textSize, LIGHTGRAY);
			DrawText("Q.K. from freesound.org", (screenWidth / 2) - MeasureText("Q.K. from freesound.org", sizes::textSize / 2), pos::credits2PosY6, sizes::textSize, LIGHTGRAY);
			DrawText("CERB14 from freesound.org", (screenWidth / 2) - MeasureText("CERB14 from freesound.org", sizes::textSize / 2), pos::credits2PosY7, sizes::textSize, LIGHTGRAY);
			DrawText("edwardszakal from freesound.org", (screenWidth / 2) - MeasureText("edwardszakal from freesound.org", sizes::textSize / 2), pos::credits2PosY8, sizes::textSize, LIGHTGRAY);

			DrawTexture(textures::leftArrow, pos::upButtons, pos::upButtons, leftArrowButtonStatus);
			DrawTexture(textures::menuButton2, pos::credits2UpButton, pos::upButtons, menuButtonStatus);
		}

		void deinit()
		{
			UnloadTexture(textures::leftArrow);
		}
	}
}