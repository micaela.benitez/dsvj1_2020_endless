#ifndef CREDITS_H
#define CREDITS_H

#include "scenes/gameplay/gameplay.h"

namespace endless
{
	namespace credits
	{
		void init();
		void update();
		void draw();
		void deinit();
	}
}

#endif