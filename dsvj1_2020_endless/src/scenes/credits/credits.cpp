#include "credits.h"

using namespace endless;
using namespace gameplay;

namespace endless
{
	namespace credits
	{
		static Color menuButtonStatus = WHITE;
		static Color rightArrowButtonStatus = WHITE;

		void init()
		{
			textures::rightArrow = LoadTexture("res/raw/textures/rightarrow.png");
			textures::rightArrow.width = sizes::arrowsSize;
			textures::rightArrow.height = sizes::arrowsSize;
		}

		void update()
		{
			mousePoint = GetMousePosition();

			if (CheckCollisionPointRec(mousePoint, { pos::upButtons, pos::upButtons, (float)textures::menuButton2.width, (float)textures::menuButton2.height }))
			{
				menuButtonStatus = DARKGRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					game::currentScene = game::Scene::MENU;
				}
			}
			else menuButtonStatus = WHITE;

			if (CheckCollisionPointRec(mousePoint, { pos::creditsUpButton, pos::upButtons, (float)textures::rightArrow.width, (float)textures::rightArrow.height }))
			{
				rightArrowButtonStatus = DARKGRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					game::currentScene = game::Scene::CREDITS2;
				}
			}
			else rightArrowButtonStatus = WHITE;

			backgroundMotion();

			audio::menuAudio();
			sounds.button = false;
		}

		void draw()
		{
			drawingBackground();

			DrawText("Credits", (screenWidth / 2) - MeasureText("Credits", sizes::textSize5 / 2), pos::creditsTitlePosY, sizes::textSize5, WHITE);
			DrawText("Space runner V 2.0", (screenWidth / 2) - MeasureText("Space runner V 0.1", sizes::textSize / 2), pos::versionPosY, sizes::textSize, WHITE);

			DrawText("Programmer", (screenWidth / 2) - MeasureText("Programmer", sizes::textSize2 / 2), pos::creditsPosY, sizes::textSize2, WHITE);
			DrawText("Micaela Luz Benitez", (screenWidth / 2) - MeasureText("Micaela Luz Benitez", sizes::textSize / 2), pos::creditsPosY2, sizes::textSize, LIGHTGRAY);

			DrawText("Textures", (screenWidth / 2) - MeasureText("Textures", sizes::textSize2 / 2), pos::creditsPosY3, sizes::textSize2, WHITE);
			DrawText("Micaela Luz Benitez", (screenWidth / 2) - MeasureText("Micaela Luz Benitez", sizes::textSize / 2), pos::creditsPosY4, sizes::textSize, LIGHTGRAY);
			DrawText("Nicolas Jimenez", (screenWidth / 2) - MeasureText("Nicolas Jimenez", sizes::textSize / 2), pos::creditsPosY5, sizes::textSize, LIGHTGRAY);
			DrawText("Bestmountain pix from kindpng.com", (screenWidth / 2) - MeasureText("Bestmountain pix from kindpng.com", sizes::textSize / 2), pos::creditsPosY6, sizes::textSize, LIGHTGRAY);
			DrawText("Anna Mozgovets from shutterstock.com", (screenWidth / 2) - MeasureText("Anna Mozgovets from shutterstock.com", sizes::textSize / 2), pos::creditsPosY7, sizes::textSize, LIGHTGRAY);
			DrawText("Stockunlimited from pixlr.com", (screenWidth / 2) - MeasureText("Stockunlimited from pixlr.com", sizes::textSize / 2), pos::creditsPosY8, sizes::textSize, LIGHTGRAY);
			DrawText("Jessiehancoc1115 from freepng.es", (screenWidth / 2) - MeasureText("Jessiehancoc1115 from freepng.es", sizes::textSize / 2), pos::creditsPosY9, sizes::textSize, LIGHTGRAY);
			DrawText("Zuzy Roux from pinterest.de", (screenWidth / 2) - MeasureText("Zuzy Roux from pinterest.de", sizes::textSize / 2), pos::creditsPosY10, sizes::textSize, LIGHTGRAY);
			DrawText("pngwing.com", (screenWidth / 2) - MeasureText("pngwing.com", sizes::textSize / 2), pos::creditsPosY11, sizes::textSize, LIGHTGRAY);

			DrawTexture(textures::menuButton2, pos::upButtons, pos::upButtons, menuButtonStatus);
			DrawTexture(textures::rightArrow, pos::creditsUpButton, pos::upButtons, rightArrowButtonStatus);
			
		}

		void deinit()
		{
			UnloadTexture(textures::rightArrow);
		}
	}
}