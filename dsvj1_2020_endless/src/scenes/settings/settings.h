#ifndef SETTINGS_H
#define SETTINGS_H

#include "scenes/gameplay/gameplay.h"
#include "functions_library/keys/keys.h"

namespace endless
{
	namespace settings
	{
		struct Keys
		{
			int up = KEY_UP;
			int down = KEY_DOWN;
			int left = KEY_LEFT;
			int right = KEY_RIGHT;
			int bend = KEY_ENTER;
			int jump = KEY_SPACE;
			int shoot = KEY_S;

			int upNumber = 26;
			int downNumber = 27;
			int leftNumber = 28;
			int rightNumber = 29;
			int bendNumber = 30;
			int jumpNumber = 31;
			int shootNumber = 18;
		};

		extern Keys key;

		extern Color leftArrowButtonStatus;
		extern Color rightArrowButtonStatus;
		extern Color upButtonStatus;
		extern Color downButtonStatus;
		extern Color rightButtonStatus;
		extern Color leftButtonStatus;
		extern Color jumpButtonStatus;
		extern Color bendButtonStatus;
		extern Color shootButtonStatus;

		void init();
		void update();
		void draw();
		void deinit();
	}
}

#endif