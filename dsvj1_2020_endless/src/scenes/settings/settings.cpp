#include "settings.h"

using namespace endless;
using namespace gameplay;

namespace endless
{
	namespace settings
	{
		Keys key;

		Color leftArrowButtonStatus = WHITE;
		Color rightArrowButtonStatus = WHITE;
		static Color menuButtonStatus = WHITE;
		static Color defaultButtonStatus = WHITE;

		Color upButtonStatus = WHITE;
		Color downButtonStatus = WHITE;
		Color rightButtonStatus = WHITE;
		Color leftButtonStatus = WHITE;
		Color jumpButtonStatus = WHITE;
		Color bendButtonStatus = WHITE;
		Color shootButtonStatus = WHITE;
		static bool upButtonActive = false;
		static bool downButtonActive = false;
		static bool rightButtonActive = false;
		static bool leftButtonActive = false;
		static bool jumpButtonActive = false;
		static bool bendButtonActive = false;
		static bool shootButtonActive = false;

		void init()
		{
			textures::defaultControls = LoadTexture("res/raw/textures/defaultcontrols.png");
			textures::defaultControls.width = 170;
			textures::defaultControls.height = 80;
		}

		void update()
		{
			mousePoint = GetMousePosition();

			if (CheckCollisionPointRec(mousePoint, { pos::upButtons, pos::upButtons, (float)textures::menuButton2.width, (float)textures::menuButton2.height }))
			{
				menuButtonStatus = DARKGRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					game::currentScene = game::Scene::MENU;
				}
			}
			else menuButtonStatus = WHITE;


			if (CheckCollisionPointRec(mousePoint, { pos::settingPosX, pos::upButtons, (float)textures::defaultControls.width, (float)textures::defaultControls.height }))
			{
				defaultButtonStatus = DARKGRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					upButtonStatus = WHITE;
					downButtonStatus = WHITE;
					leftButtonStatus = WHITE;
					rightButtonStatus = WHITE;
					bendButtonStatus = WHITE;
					jumpButtonStatus = WHITE;
					shootButtonStatus = WHITE;
					upButtonActive = false;
					downButtonActive = false;
					leftButtonActive = false;
					rightButtonActive = false;
					bendButtonActive = false;
					jumpButtonActive = false;
					shootButtonActive = false;
					key.up = KEY_UP;
					key.down = KEY_DOWN;
					key.left = KEY_LEFT;
					key.right = KEY_RIGHT;
					key.bend = KEY_ENTER;
					key.jump = KEY_SPACE;
					key.shoot = KEY_S;
				}
			}
			else defaultButtonStatus = WHITE;

			if (CheckCollisionPointRec(mousePoint, { pos::settingsPosX, pos::settingsPosY, sizes::recSize, sizes::recSize }) && IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
			{
				sounds.button = true;
				upButtonStatus = GRAY;
				downButtonStatus = WHITE;
				leftButtonStatus = WHITE;
				rightButtonStatus = WHITE;
				bendButtonStatus = WHITE;
				jumpButtonStatus = WHITE;
				shootButtonStatus = WHITE;
				upButtonActive = true;
				downButtonActive = false;
				leftButtonActive = false;
				rightButtonActive = false;
				bendButtonActive = false;
				jumpButtonActive = false;
				shootButtonActive = false;
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::settingsPosX, pos::settingsPosY2, sizes::recSize, sizes::recSize }) && IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
			{
				sounds.button = true;
				upButtonStatus = WHITE;
				downButtonStatus = GRAY;
				leftButtonStatus = WHITE;
				rightButtonStatus = WHITE;
				bendButtonStatus = WHITE;
				jumpButtonStatus = WHITE;
				shootButtonStatus = WHITE;
				upButtonActive = false;
				downButtonActive = true;
				leftButtonActive = false;
				rightButtonActive = false;
				bendButtonActive = false;
				jumpButtonActive = false;
				shootButtonActive = false;
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::settingsPosX2, pos::settingsPosY2, sizes::recSize, sizes::recSize }) && IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
			{
				sounds.button = true;
				upButtonStatus = WHITE;
				downButtonStatus = WHITE;
				leftButtonStatus = GRAY;
				rightButtonStatus = WHITE;
				bendButtonStatus = WHITE;
				jumpButtonStatus = WHITE;
				shootButtonStatus = WHITE;
				upButtonActive = false;
				downButtonActive = false;
				leftButtonActive = true;
				rightButtonActive = false;
				bendButtonActive = false;
				jumpButtonActive = false;
				shootButtonActive = false;
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::settingsPosX3, pos::settingsPosY2, sizes::recSize, sizes::recSize }) && IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
			{
				sounds.button = true;
				upButtonStatus = WHITE;
				downButtonStatus = WHITE;
				leftButtonStatus = WHITE;
				rightButtonStatus = GRAY;
				bendButtonStatus = WHITE;
				jumpButtonStatus = WHITE;
				shootButtonStatus = WHITE;
				upButtonActive = false;
				downButtonActive = false;
				leftButtonActive = false;
				rightButtonActive = true;
				bendButtonActive = false;
				jumpButtonActive = false;
				shootButtonActive = false;
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::settingsPosX, pos::settingsPosY3, sizes::recSize, sizes::recSize }) && IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
			{
				sounds.button = true;
				upButtonStatus = WHITE;
				downButtonStatus = WHITE;
				leftButtonStatus = WHITE;
				rightButtonStatus = WHITE;
				bendButtonStatus = GRAY;
				jumpButtonStatus = WHITE;
				shootButtonStatus = WHITE;
				upButtonActive = false;
				downButtonActive = false;
				leftButtonActive = false;
				rightButtonActive = false;
				bendButtonActive = true;
				jumpButtonActive = false;
				shootButtonActive = false;
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::settingsPosX2, pos::settingsPosY3, sizes::recSize, sizes::recSize }) && IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
			{
				sounds.button = true;
				upButtonStatus = WHITE;
				downButtonStatus = WHITE;
				leftButtonStatus = WHITE;
				rightButtonStatus = WHITE;
				bendButtonStatus = WHITE;
				jumpButtonStatus = GRAY;
				shootButtonStatus = WHITE;
				upButtonActive = false;
				downButtonActive = false;
				leftButtonActive = false;
				rightButtonActive = false;
				bendButtonActive = false;
				jumpButtonActive = true;
				shootButtonActive = false;
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::settingsPosX3, pos::settingsPosY3, sizes::recSize, sizes::recSize }) && IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
			{
				sounds.button = true;
				upButtonStatus = WHITE;
				downButtonStatus = WHITE;
				leftButtonStatus = WHITE;
				rightButtonStatus = WHITE;
				bendButtonStatus = WHITE;
				jumpButtonStatus = WHITE;
				shootButtonStatus = GRAY;
				upButtonActive = false;
				downButtonActive = false;
				leftButtonActive = false;
				rightButtonActive = false;
				bendButtonActive = false;
				jumpButtonActive = false;
				shootButtonActive = true;
			}
			else if (IsKeyPressed(KEY_ENTER))
			{
				sounds.button = true;
				upButtonStatus = WHITE;
				downButtonStatus = WHITE;
				leftButtonStatus = WHITE;
				rightButtonStatus = WHITE;
				bendButtonStatus = WHITE;
				jumpButtonStatus = WHITE;
				shootButtonStatus = WHITE;
				upButtonActive = false;
				downButtonActive = false;
				leftButtonActive = false;
				rightButtonActive = false;
				bendButtonActive = false;
				jumpButtonActive = false;
				shootButtonActive = false;
			}

			if (upButtonActive) changeKey(key.up, key.down, key.left, key.right, key.bend, key.jump, key.shoot, key.upNumber);
			else if (downButtonActive) changeKey(key.down, key.up, key.left, key.right, key.bend, key.jump, key.shoot, key.downNumber);
			else if (leftButtonActive) changeKey(key.left, key.up, key.down, key.right, key.bend, key.jump, key.shoot, key.leftNumber);
			else if (rightButtonActive) changeKey(key.right, key.up, key.left, key.down, key.bend, key.jump, key.shoot, key.rightNumber);
			else if (bendButtonActive) changeKey(key.bend, key.up, key.left, key.right, key.down, key.jump, key.shoot, key.bendNumber);
			else if (jumpButtonActive) changeKey(key.jump, key.up, key.left, key.right, key.bend, key.down, key.shoot, key.jumpNumber);
			else if (shootButtonActive) changeKey(key.shoot, key.up, key.left, key.right, key.bend, key.jump, key.down, key.shootNumber);

			backgroundMotion();
			audio::menuAudio();
			sounds.button = false;
		}

		void draw()
		{
			drawingBackground();

			DrawText("Seetings", (screenWidth / 2) - MeasureText("Settings", sizes::textSize5 / 2), pos::settingsTitlePosY, sizes::textSize5, WHITE);

			drawKeys();

			if (upButtonActive || downButtonActive || rightButtonActive || leftButtonActive || jumpButtonActive || bendButtonActive || shootButtonActive)
			{
				DrawText("Press ENTER when you finish choosing the key", (screenWidth / 2) - MeasureText("Press ENTER when you finish choosing the key", sizes::textSize / 2), pos::settingsPosY8, sizes::textSize, LIGHTGRAY);
			}
			else DrawText("Press the KEY you want to change", (screenWidth / 2) - MeasureText("Press the KEY you want to change", sizes::textSize / 2), pos::settingsPosY8, sizes::textSize, LIGHTGRAY);

			DrawTexture(textures::leftArrow, pos::settingPosX2, pos::settingPosY, leftArrowButtonStatus);
			DrawTexture(textures::rightArrow, pos::settingPosX3, pos::settingPosY2, rightArrowButtonStatus);
			DrawTexture(textures::menuButton2, pos::upButtons, pos::upButtons, menuButtonStatus);
			DrawTexture(textures::defaultControls, pos::settingPosX, pos::upButtons, defaultButtonStatus);
		}

		void deinit()
		{
			UnloadTexture(textures::defaultControls);
		}
	}
}