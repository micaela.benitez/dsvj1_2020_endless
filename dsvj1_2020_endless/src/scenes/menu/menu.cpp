#include "menu.h"

using namespace endless;
using namespace gameplay;

namespace endless
{
	namespace menu
	{
		static Color startButtonStatus = WHITE;
		static Color instructionsButtonStatus = WHITE;
		static Color settingsButtonStatus = WHITE;
		static Color creditsButtonStatus = WHITE;
		static Color exitButtonStatus = WHITE;
		static Color audioButtonStatus = LIGHTGRAY;
		static Color resolutionButtonStatus = LIGHTGRAY;

		void init()
		{
			textures::backgroundPlanets = LoadTexture("res/raw/textures/planets.png");

			textures::startButton = LoadTexture("res/raw/textures/startbutton.png");

			textures::instructionsButton = LoadTexture("res/raw/textures/instructionsbutton.png");

			textures::settingsButton = LoadTexture("res/raw/textures/settingsbutton.png");

			textures::creditsButton = LoadTexture("res/raw/textures/creditsbutton.png");

			textures::exitButton = LoadTexture("res/raw/textures/exitbutton.png");

			textures::highScoreMenu = LoadTexture("res/raw/textures/highscoremenu.png");
			textures::highScoreMenu.width = sizes::highScoreSize;
			textures::highScoreMenu.height = sizes::highScoreSize;

			textures::audioButton = LoadTexture("res/raw/textures/audio.png");
			textures::audioButton.width = sizes::buttonsMenuSize;
			textures::audioButton.height = sizes::buttonsMenuSize;;

			textures::resolutionButton = LoadTexture("res/raw/textures/resolution.png");
			textures::resolutionButton.width = sizes::buttonsMenuSize;;
			textures::resolutionButton.height = sizes::buttonsMenuSize;;

			// Audio
			audio::loadSounds();
			audio::loadMusic();
			audio::setSoundsVolume();
			audio::setMusicVolume();
		}

		void update()
		{
			mousePoint = GetMousePosition();

			if (CheckCollisionPointRec(mousePoint, { pos::menuPosX, pos::menuPosY, (float)textures::startButton.width, (float)textures::startButton.height }))
			{
				startButtonStatus = DARKGRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					game::currentScene = game::Scene::GAMEPLAY;
					StopMusicStream(audio::gameMusic.menu);
					initialize();
				}
			}
			else startButtonStatus = WHITE;
			
			if (CheckCollisionPointRec(mousePoint, { pos::menuPosX, pos::menuPosY2, (float)textures::instructionsButton.width, (float)textures::instructionsButton.height }))
			{
				instructionsButtonStatus = DARKGRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					game::currentScene = game::Scene::INSTRUCTIONS;
				}
			}
			else instructionsButtonStatus = WHITE;
			
			if (CheckCollisionPointRec(mousePoint, { pos::menuPosX, pos::menuPosY3, (float)textures::settingsButton.width, (float)textures::settingsButton.height }))
			{
				settingsButtonStatus = DARKGRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					game::currentScene = game::Scene::SETTINGS;
				}
			}
			else settingsButtonStatus = WHITE;
			
			if (CheckCollisionPointRec(mousePoint, { pos::menuPosX, pos::menuPosY4, (float)textures::creditsButton.width, (float)textures::creditsButton.height }))
			{
				creditsButtonStatus = DARKGRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					game::currentScene = game::Scene::CREDITS;
				}
			}
			else creditsButtonStatus = WHITE;
			
			if (CheckCollisionPointRec(mousePoint, { pos::menuPosX, pos::menuPosY5, (float)textures::exitButton.width, (float)textures::exitButton.height }))
			{
				exitButtonStatus = DARKGRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					game::exitButton = true;
				}
			}
			else exitButtonStatus = WHITE;

			if (CheckCollisionPointRec(mousePoint, { pos::upButtons, pos::upButtons, (float)textures::audioButton.width, (float)textures::audioButton.height }))
			{
				audioButtonStatus = DARKGRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					game::currentScene = game::Scene::AUDIO;
					game::previousScene = game::Scene::MENU;
				}
			}
			else audioButtonStatus = LIGHTGRAY;

			if (CheckCollisionPointRec(mousePoint, { pos::menuPosX2, pos::upButtons, (float)textures::resolutionButton.width, (float)textures::resolutionButton.height }))
			{
				resolutionButtonStatus = DARKGRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					game::currentScene = game::Scene::RESOLUTIONS;
					game::previousScene = game::Scene::MENU;
				}
			}
			else resolutionButtonStatus = LIGHTGRAY;

			backgroundMotion();

			PlayMusicStream(audio::gameMusic.menu);
			audio::menuAudio();
			sounds.button = false;
		}

		void draw()
		{
			drawingBackground();

			DrawText("Space Runner", (screenWidth / 2) - MeasureText("Space Runner", sizes::titleSize / 2), pos::titlePoxY, sizes::titleSize, WHITE);
			DrawTexture(textures::startButton, pos::menuPosX, pos::menuPosY, startButtonStatus);
			DrawTexture(textures::instructionsButton, pos::menuPosX, pos::menuPosY2, instructionsButtonStatus);
			DrawTexture(textures::settingsButton, pos::menuPosX, pos::menuPosY3, settingsButtonStatus);
			DrawTexture(textures::creditsButton, pos::menuPosX, pos::menuPosY4, creditsButtonStatus);
			DrawTexture(textures::exitButton, pos::menuPosX, pos::menuPosY5, exitButtonStatus);

			DrawTexture(textures::audioButton, pos::upButtons, pos::upButtons, audioButtonStatus);
			DrawTexture(textures::resolutionButton, pos::menuPosX2, pos::upButtons, resolutionButtonStatus);

			DrawTexture(textures::highScoreMenu, pos::menuHighScorePosX, pos::menuHighScorePosY, WHITE);
			if (highScore > 0)
			{
				DrawText("High", pos::menuTextPosX, pos::menuTextPosY, sizes::textSize, BLACK);
				DrawText("Score", pos::menuTextPosX2, pos::menuTextPosY2, sizes::textSize, BLACK);
				DrawText(TextFormat("%06i", highScore), pos::menuTextPosX3, pos::menuTextPosY3, sizes::textSize, BLACK);
			}
			else
			{
				DrawText("You dont", pos::menuTextPosX4, pos::menuTextPosY, sizes::textSize, BLACK);
				DrawText("have a", pos::menuTextPosX5, pos::menuTextPosY2, sizes::textSize, BLACK);
				DrawText("high score", pos::menuTextPosX6, pos::menuTextPosY4, sizes::textSize, BLACK);
				DrawText("yet", pos::menuTextPosX7, pos::menuTextPosY5, sizes::textSize, BLACK);
			}

			DrawText("V 2.0", pos::versionPosX, pos::versionPosY, sizes::textSize, LIGHTGRAY);
		}

		void deinit()
		{
			UnloadTexture(textures::backgroundPlanets);
			UnloadTexture(textures::startButton);
			UnloadTexture(textures::instructionsButton);
			UnloadTexture(textures::settingsButton);
			UnloadTexture(textures::creditsButton);
			UnloadTexture(textures::exitButton);
			UnloadTexture(textures::highScoreMenu);
			UnloadTexture(textures::audioButton);
			UnloadTexture(textures::resolutionButton);

			audio::unloadSounds();
			audio::unloadMusic();
		}
	}
}