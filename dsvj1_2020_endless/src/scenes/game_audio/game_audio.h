#ifndef GAME_AUDIO_H
#define GAME_AUDIO_H

#include "scenes/gameplay/gameplay.h"

namespace endless
{
	namespace game_audio
	{
		void init();
		void update();
		void draw();
		void deinit();
	}
}

#endif