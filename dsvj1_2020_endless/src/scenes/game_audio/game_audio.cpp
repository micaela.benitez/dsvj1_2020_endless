#include "game_audio.h"

using namespace endless;
using namespace gameplay;

namespace endless
{
	namespace game_audio
	{
		static Color musicMute = GRAY;
		static Color musicLow = GRAY;
		static Color musicHigh = WHITE;
		static Color soundsMute = GRAY;
		static Color soundsLow = GRAY;
		static Color soundsHigh = WHITE;

		static Color quitButtonStatus = WHITE;

		void init()
		{
			textures::quitButton = LoadTexture("res/raw/textures/quit.png");
			textures::quitButton.width = 70;
			textures::quitButton.height = 70;
		}

		void update()
		{
			mousePoint = GetMousePosition();

			if (CheckCollisionPointRec(mousePoint, { pos::audioRecPosX, pos::audioRecPosY, sizes::recWidth, sizes::recHeight }))
			{
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					musicMute = WHITE;
					musicLow = GRAY;
					musicHigh = GRAY;
					audio::musicVolume = 0.0f;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::audioRecPosX2, pos::audioRecPosY, sizes::recWidth, sizes::recHeight }))
			{
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					musicMute = GRAY;
					musicLow = WHITE;
					musicHigh = GRAY;
					audio::musicVolume = 0.2f;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::audioRecPosX3, pos::audioRecPosY, sizes::recWidth, sizes::recHeight }))
			{
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					musicMute = GRAY;
					musicLow = GRAY;
					musicHigh = WHITE;
					audio::musicVolume = 0.4f;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::audioRecPosX4, pos::audioRecPosY2, sizes::recWidth, sizes::recHeight }))
			{
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					soundsMute = WHITE;
					soundsLow = GRAY;
					soundsHigh = GRAY;
					audio::soundVolume = 0.0f;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::audioRecPosX5, pos::audioRecPosY2, sizes::recWidth, sizes::recHeight }))
			{
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					soundsMute = GRAY;
					soundsLow = WHITE;
					soundsHigh = GRAY;
					audio::soundVolume = 0.5f;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::audioRecPosX6, pos::audioRecPosY2, sizes::recWidth, sizes::recHeight }))
			{
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					soundsMute = GRAY;
					soundsLow = GRAY;
					soundsHigh = WHITE;
					audio::soundVolume = 5.0f;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::quitButtonPosX, pos::quitButtonPosY, (float)textures::quitButton.width, (float)textures::quitButton.height }))
			{
				quitButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					game::currentScene = game::previousScene;
				}
			}
			else
			{
				quitButtonStatus = WHITE;
			}

			if (game::previousScene == game::Scene::GAMEPLAY)
			{
				PlayMusicStream(audio::gameMusic.gameplay);
				audio::gameplayAudio();
				audio::setSoundsVolume();
				audio::setMusicVolume();
			}
			else
			{
				PlayMusicStream(audio::gameMusic.menu);
				audio::menuAudio();
				audio::setSoundsVolume();
				audio::setMusicVolume();
			}

			backgroundMotion();
			sounds.button = false;
		}

		void draw()
		{
			drawingBackground();

			DrawText("Audio", (screenWidth / 2) - MeasureText("Audio", sizes::textSize5 / 2), pos::audioPosY, sizes::textSize5, WHITE);

			DrawText("Music", pos::audioPosX, pos::audioPosY2, sizes::textSize2, WHITE);
			DrawText("Mute", pos::audioPosX2, pos::audioPosY3, sizes::textSize2, musicMute);
			DrawText("Low", pos::audioPosX3, pos::audioPosY3, sizes::textSize2, musicLow);
			DrawText("High", pos::audioPosX4, pos::audioPosY3, sizes::textSize2, musicHigh);
			DrawRectangleLinesEx({ pos::audioRecPosX, pos::audioRecPosY, sizes::recWidth , sizes::recHeight }, sizes::recLinesSize, musicMute);
			DrawRectangleLinesEx({ pos::audioRecPosX2, pos::audioRecPosY, sizes::recWidth , sizes::recHeight }, sizes::recLinesSize, musicLow);
			DrawRectangleLinesEx({ pos::audioRecPosX3, pos::audioRecPosY, sizes::recWidth , sizes::recHeight }, sizes::recLinesSize, musicHigh);

			DrawText("Sounds", pos::audioPosX5, pos::audioPosY4, sizes::textSize2, WHITE);
			DrawText("Mute", pos::audioPosX6, pos::audioPosY5, sizes::textSize2, soundsMute);
			DrawText("Low", pos::audioPosX7, pos::audioPosY5, sizes::textSize2, soundsLow);
			DrawText("High", pos::audioPosX8, pos::audioPosY5, sizes::textSize2, soundsHigh);
			DrawRectangleLinesEx({ pos::audioRecPosX4, pos::audioRecPosY2, sizes::recWidth , sizes::recHeight }, sizes::recLinesSize, soundsMute);
			DrawRectangleLinesEx({ pos::audioRecPosX5, pos::audioRecPosY2, sizes::recWidth , sizes::recHeight }, sizes::recLinesSize, soundsLow);
			DrawRectangleLinesEx({ pos::audioRecPosX6, pos::audioRecPosY2, sizes::recWidth , sizes::recHeight }, sizes::recLinesSize, soundsHigh);

			DrawTexture(textures::quitButton, (int)pos::quitButtonPosX, (int)pos::quitButtonPosY, quitButtonStatus);
		}

		void deinit()
		{
			UnloadTexture(textures::quitButton);
		}
	}
}