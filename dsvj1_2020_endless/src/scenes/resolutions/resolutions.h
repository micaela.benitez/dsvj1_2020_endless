#ifndef RESOLUTIONS_H
#define RESOLUTIONS_H

#include "scenes/gameplay/gameplay.h"

namespace endless
{
	namespace resolutions
	{
		void init();
		void update();
		void draw();
		void deinit();
	}
}

#endif