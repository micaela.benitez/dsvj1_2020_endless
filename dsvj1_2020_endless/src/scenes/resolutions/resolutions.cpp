#include "resolutions.h"

using namespace endless;
using namespace gameplay;

namespace endless
{
	namespace resolutions
	{
		static Color screenWidth900 = GRAY;
		static Color screenWidth1200 = WHITE;
		static Color screenWidth1500 = GRAY;
		static Color screenHeight800 = GRAY;
		static Color screenHeight900 = WHITE;
		static Color screenHeight1000 = GRAY;

		static Color quitButtonStatus = WHITE;

		void init()
		{
		}

		void update()
		{
			mousePoint = GetMousePosition();

			if (CheckCollisionPointRec(mousePoint, { pos::resolutionsRecPosX, pos::resolutionsRecPosY, sizes::rec2Width, sizes::recHeight }))
			{
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					screenWidth900 = WHITE;
					screenWidth1200 = GRAY;
					screenWidth1500 = GRAY;
					screenWidth = 900;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::resolutionsRecPosX2, pos::resolutionsRecPosY, sizes::rec2Width, sizes::recHeight }))
			{
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					screenWidth900 = GRAY;
					screenWidth1200 = WHITE;
					screenWidth1500 = GRAY;
					screenWidth = 1200;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::resolutionsRecPosX3, pos::resolutionsRecPosY, sizes::rec2Width, sizes::recHeight }))
			{
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					screenWidth900 = GRAY;
					screenWidth1200 = GRAY;
					screenWidth1500 = WHITE;
					screenWidth = 1500;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::resolutionsRecPosX4, pos::resolutionsRecPosY2, sizes::rec2Width, sizes::recHeight }))
			{
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					screenHeight800 = WHITE;
					screenHeight900 = GRAY;
					screenHeight1000 = GRAY;
					screenHeight = 800;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::resolutionsRecPosX5, pos::resolutionsRecPosY2, sizes::rec2Width, sizes::recHeight }))
			{
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					screenHeight800 = GRAY;
					screenHeight900 = WHITE;
					screenHeight1000 = GRAY;
					screenHeight = 900;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::resolutionsRecPosX6, pos::resolutionsRecPosY2, sizes::rec2Width, sizes::recHeight }))
			{
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					screenHeight800 = GRAY;
					screenHeight900 = GRAY;
					screenHeight1000 = WHITE;
					screenHeight = 1000;
				}
			}
			else if (CheckCollisionPointRec(mousePoint, { pos::quitButtonPosX, pos::quitButtonPosY, (float)textures::quitButton.width, (float)textures::quitButton.height }))
			{
				quitButtonStatus = GRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					game::currentScene = game::previousScene;
				}
			}
			else
			{
				quitButtonStatus = WHITE;
			}

			pos::updateVariables();

			textures::background.height = (int)screenHeight;
			textures::background2.height = (int)screenHeight;
			textures::background3.height = (int)screenHeight;
			textures::background4.height = (int)screenHeight;

			if (game::previousScene == game::Scene::GAMEPLAY)
			{
				PlayMusicStream(audio::gameMusic.gameplay);
				audio::gameplayAudio();
				audio::setSoundsVolume();
				audio::setMusicVolume();
			}
			else
			{
				PlayMusicStream(audio::gameMusic.menu);
				audio::menuAudio();
				audio::setSoundsVolume();
				audio::setMusicVolume();
			}

			backgroundMotion();
			sounds.button = false;
		}

		void draw()
		{
			drawingBackground();

			DrawText("Resolution", (screenWidth / 2) - MeasureText("Resolution", sizes::textSize5 / 2), pos::resolutionsPosY, sizes::textSize5, WHITE);

			DrawText("Width", pos::resolutionsPosX, pos::resolutionsPosY2, sizes::textSize2, WHITE);
			DrawText("900", pos::resolutionsPosX2, pos::resolutionsPosY3, sizes::textSize2, screenWidth900);
			DrawText("1200", pos::resolutionsPosX3, pos::resolutionsPosY3, sizes::textSize2, screenWidth1200);
			DrawText("1500", pos::resolutionsPosX4, pos::resolutionsPosY3, sizes::textSize2, screenWidth1500);
			DrawRectangleLinesEx({ pos::resolutionsRecPosX, pos::resolutionsRecPosY, sizes::rec2Width , sizes::recHeight }, sizes::recLinesSize, screenWidth900);
			DrawRectangleLinesEx({ pos::resolutionsRecPosX2, pos::resolutionsRecPosY, sizes::rec2Width , sizes::recHeight }, sizes::recLinesSize, screenWidth1200);
			DrawRectangleLinesEx({ pos::resolutionsRecPosX3, pos::resolutionsRecPosY, sizes::rec2Width , sizes::recHeight }, sizes::recLinesSize, screenWidth1500);

			DrawText("Height", pos::resolutionsPosX5, pos::resolutionsPosY4, sizes::textSize2, WHITE);
			DrawText("800", pos::resolutionsPosX6, pos::resolutionsPosY5, sizes::textSize2, screenHeight800);
			DrawText("900", pos::resolutionsPosX7, pos::resolutionsPosY5, sizes::textSize2, screenHeight900);
			DrawText("1000", pos::resolutionsPosX8, pos::resolutionsPosY5, sizes::textSize2, screenHeight1000);
			DrawRectangleLinesEx({ pos::resolutionsRecPosX4, pos::resolutionsRecPosY2, sizes::rec2Width , sizes::recHeight }, sizes::recLinesSize, screenHeight800);
			DrawRectangleLinesEx({ pos::resolutionsRecPosX5, pos::resolutionsRecPosY2, sizes::rec2Width , sizes::recHeight }, sizes::recLinesSize, screenHeight900);
			DrawRectangleLinesEx({ pos::resolutionsRecPosX6, pos::resolutionsRecPosY2, sizes::rec2Width , sizes::recHeight }, sizes::recLinesSize, screenHeight1000);

			DrawTexture(textures::quitButton, (int)pos::quitButtonPosX, (int)pos::quitButtonPosY, quitButtonStatus);
		}

		void deinit()
		{
		}
	}
}
