#include "gameplay.h"

namespace endless
{
	namespace gameplay
	{
		using namespace pos;
		
		int screenWidth = 1200;
		int screenHeight = 900;

		Vector2 mousePoint = { 0.0f, 0.0f };

		Entitie typeOfEntitie = Entitie::SHIP;

		const int maxShips = maximumShips;
		const int maxStones = maximumStones;
		const int maxStones2 = maximumStones2;
		const int maxShots = maximumShots;
		const int maxFlags = maximumFlags;
		const int maxCoins = maximumCoins;
		const int maxMoon = maximumMoon;
		int numShip = -1;
		int numStone = -1;
		int numStone2 = -1;
		int numShot = -1;
		int numFlag = -1;
		int numCoin = -1;
		int numMoon = -1;

		Character character;
		Entities ship[maximumShips];
		Entities stone[maximumStones];
		Entities stone2[maximumStones2];
		Entities shot[maximumShots];
		Entities flag[maximumFlags];
		Entities coin[maximumCoins];
		Entities moon[maximumMoon];

		bool pause = true;

		int timer = 0;
		float timerCharacter = 0.0f;
		int frameCharacter = 0;
		float timerFlags = 0.0f;
		int frameFlags = 0;
		float timerCoins = 0.0f;
		int frameCoins = 0;
		float timerMoon = 0.0f;
		int frameMoon = 0;

		int actualScore = 0;
		int highScore = 0;

		float frameWidth = 0;
		int maxFrames = 0;
		float frameWidth1 = 0;
		int maxFrames1 = 0;
		float frameWidth2 = 0;
		int maxFrames2 = 0;
		float frameWidth3 = 0;
		int maxFrames3 = 0;

		int cantShots = 0;

		int timerSpeed = 1000;
		int auxTimerSpeed = 0;
		float speedModifier = 1.0f;

		int periodTimeLowerWay = 0;
		int periodTimeMiddleWay = 0;
		int periodTimeLastWay = 0;

		Sounds sounds;

		int timerBackground;

		Color audioButtonStatus = WHITE;
		static Color menuButtonStatus = WHITE;
		static Color exitButtonStatus = WHITE;

		void init()
		{			
			// Textures
			textures::background = LoadTexture("res/raw/textures/background.png");
			textures::background.height = (int)screenHeight;

			textures::background2 = LoadTexture("res/raw/textures/background2.png");
			textures::background2.height = (int)screenHeight;

			textures::background3 = LoadTexture("res/raw/textures/background3.png");
			textures::background3.height = (int)screenHeight;

			textures::background4 = LoadTexture("res/raw/textures/background4.png");
			textures::background4.height = (int)screenHeight;

			textures::stone = LoadTexture("res/raw/textures/stone.png");
			textures::stone.width = sizes::stoneWidth;
			textures::stone.height = sizes::stoneHeight;

			textures::stone2 = LoadTexture("res/raw/textures/stone2.png");
			textures::stone2.width = sizes::stone2Size;
			textures::stone2.height = sizes::stone2Size;

			textures::ship = LoadTexture("res/assets/textures/ship.png");
			textures::ship.width = sizes::shipWidth;
			textures::ship.height = sizes::shipHeight;

			textures::character = LoadTexture("res/raw/textures/astronaut.png");
			textures::character.width = sizes::characterWidth;
			textures::character.height = sizes::characterHeight;

			textures::characterJump = LoadTexture("res/raw/textures/astronautjump.png");
			textures::characterJump.width = sizes::characterWidth2;
			textures::characterJump.height = sizes::characterHeight;

			textures::characterBend = LoadTexture("res/raw/textures/astronautbend.png");
			textures::characterBend.width = sizes::characterWidth2;
			textures::characterBend.height = sizes::characterHeight;

			textures::characterShooter = LoadTexture("res/raw/textures/astronautshooter.png");
			textures::characterShooter.width = sizes::characterWidth3;
			textures::characterShooter.height = sizes::characterHeight;

			textures::characterNormal = LoadTexture("res/raw/textures/astronautnormal.png");
			textures::characterNormal.width = sizes::characterWidth2;
			textures::characterNormal.height = sizes::characterHeight;

			textures::shots = LoadTexture("res/raw/textures/fire.png");

			textures::menuButton2 = LoadTexture("res/raw/textures/menubutton2.png");
			textures::menuButton2.width = sizes::buttonsGameplayWidth;
			textures::menuButton2.height = sizes::buttonsGameplayHeight;

			textures::exitButton2 = LoadTexture("res/raw/textures/exitbutton2.png");
			textures::exitButton2.width = sizes::buttonsGameplayWidth;
			textures::exitButton2.height = sizes::buttonsGameplayHeight;

			textures::flags = LoadTexture("res/assets/textures/flags.png");

			textures::coins = LoadTexture("res/assets/textures/coins.png");
			textures::coins.width = sizes::coinsSize * 6;
			textures::coins.height = sizes::coinsSize;

			textures::moon = LoadTexture("res/assets/textures/moon.png");

			// Character
			character.position = { pos::initialWayCharacterPosX, middleWayCharacter };
			character.actualWay = character.position.y;
			character.speedJump = speed::speedJumpCharacter;
			character.active = true;
			character.jump = false;
			character.bend = false;
			character.shooter = false;
			character.changeOfWay = false;

			// Shots
			for (int i = 0; i < maximumShots; i++)
			{
				shot[i].texture = textures::shots;
				shot[i].active = false;
			}

			// Ships
			for (int i = 0; i < maxShips; i++)
			{
				ship[i].texture = textures::ship;
				ship[i].scrollingPosX = screenWidth;
				ship[i].active = false;				
			}

			// Stones
			for (int i = 0; i < maxStones; i++)
			{
				stone[i].texture = textures::stone;
				stone[i].scrollingPosX = screenWidth;
				stone[i].active = false;
			}

			// Stones 2
			for (int i = 0; i < maxStones2; i++)
			{
				stone2[i].texture = textures::stone2;
				stone2[i].scrollingPosX = screenWidth;
				stone2[i].active = false;
			}

			// Flags
			for (int i = 0; i < maxFlags; i++)
			{
				flag[i].texture = textures::flags;
				flag[i].scrollingPosX = screenWidth;
				flag[i].active = false;
			}

			// Coins
			for (int i = 0; i < maxCoins; i++)
			{
				coin[i].texture = textures::coins;
				coin[i].scrollingPosX = screenWidth;
				coin[i].active = false;
			}

			// Moon
			for (int i = 0; i < maxMoon; i++)
			{
				moon[i].texture = textures::moon;
				moon[i].scrollingPosX = screenWidth;
				moon[i].active = false;
			}
		}

		void initialize()
		{
			timer = 0;
			timerSpeed = 1000;
			auxTimerSpeed = 0;
			speedModifier = 1.0f;
			timerBackground = 1000;

			actualScore = 0;
			numShip = -1;
			numStone = -1;
			numStone2 = -1;
			numShot = -1;
			numFlag = -1;
			numCoin = -1;
			numMoon = -1;

			pause = true;

			// Character
			character.position = { pos::initialWayCharacterPosX, middleWayCharacter };
			character.actualWay = character.position.y;
			character.speedJump = speed::speedJumpCharacter;
			character.active = true;
			character.jump = false;
			character.bend = false;
			character.shooter = false;
			character.changeOfWay = false;

			// Shots
			for (int i = 0; i < maximumShots; i++) shot[i].active = false;

			// Ships
			for (int i = 0; i < maxShips; i++)
			{
				ship[i].scrollingPosX = screenWidth;
				ship[i].active = false;
			}

			// Stones
			for (int i = 0; i < maxStones; i++)
			{
				stone[i].scrollingPosX = screenWidth;
				stone[i].active = false;
			}

			// Stones 2
			for (int i = 0; i < maxStones2; i++)
			{
				stone2[i].scrollingPosX = screenWidth;
				stone2[i].active = false;
			}

			// Flags
			for (int i = 0; i < maxFlags; i++)
			{
				flag[i].scrollingPosX = screenWidth;
				flag[i].active = false;
			}

			// Coins
			for (int i = 0; i < maxCoins; i++)
			{
				coin[i].scrollingPosX = screenWidth;
				coin[i].active = false;
			}

			// Moon
			for (int i = 0; i < maxMoon; i++)
			{
				moon[i].scrollingPosX = screenWidth;
				moon[i].active = false;
			}
		}

		void update()
		{
			mousePoint = GetMousePosition();

			if (actualScore > highScore) highScore = actualScore;   // Score

			if (IsKeyPressed(KEY_P))   // Pause
			{
				pause = (pause == false) ? true : false;
				sounds.button = true;
			}

			if (character.active && !pause)
			{				
				framesMotion();
				backgroundMotion();	
				characterMotion();
				entitiesMotion();

				collisionShip();
				collisionStone();
				collisionStone2();
				collisionFlag();
				collisionCoin();
				collisionMoon();

				speedModifierTimer();
				backgroundModifierTimer();
			}		
			else if (timer == 0)
			{
				framesMotion();
				characterMotion();
			}
			else if (pause)
			{
				if (CheckCollisionPointRec(mousePoint, { pos::gameplayCenterButton, pos::gameplayDownButton, (float)textures::audioButton.width, (float)textures::audioButton.height }))
				{
					audioButtonStatus = DARKGRAY;
					if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
					{
						sounds.button = true;
						game::currentScene = game::Scene::AUDIO;
						game::previousScene = game::Scene::GAMEPLAY;
					}
				}
				else audioButtonStatus = WHITE;
			}

			if (CheckCollisionPointRec(mousePoint, { pos::upButtons, pos::upButtons, (float)textures::menuButton2.width, (float)textures::menuButton2.height }))
			{
				menuButtonStatus = DARKGRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					game::currentScene = game::Scene::MENU;
					StopMusicStream(audio::gameMusic.gameplay);
				}
			}
			else menuButtonStatus = WHITE;

			if (CheckCollisionPointRec(mousePoint, { pos::gameplayUpButton, pos::upButtons, (float)textures::exitButton2.width, (float)textures::exitButton2.height }))
			{
				exitButtonStatus = DARKGRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					game::exitButton = true;
				}
			}
			else exitButtonStatus = WHITE;

			PlayMusicStream(audio::gameMusic.gameplay);
			audio::gameplayAudio();
			sounds.button = false;
			sounds.jump = false;
			sounds.pickup = false;
			sounds.shoot = false;
			sounds.stone_broken = false;
			sounds.loser = false;
		}

		void draw()
		{			
			drawingBackground();			
			drawingObstacles();
			drawingPickups();
			drawingCharacter();
			drawingShots();
			drawingScreenDetails();
			
			DrawTexture(textures::menuButton2, pos::upButtons, pos::upButtons, menuButtonStatus);
			DrawTexture(textures::exitButton2, pos::gameplayUpButton, pos::upButtons, exitButtonStatus);
		}

		void deinit()
		{
			UnloadTexture(textures::background);
			UnloadTexture(textures::background2);
			UnloadTexture(textures::ship);
			UnloadTexture(textures::stone);
			UnloadTexture(textures::stone2);
			UnloadTexture(textures::character);
			UnloadTexture(textures::characterJump);
			UnloadTexture(textures::characterBend);
			UnloadTexture(textures::characterShooter);
			UnloadTexture(textures::characterNormal);
			UnloadTexture(textures::shots);
			UnloadTexture(textures::menuButton2);
			UnloadTexture(textures::exitButton2);
			UnloadTexture(textures::flags);
			UnloadTexture(textures::coins);
			UnloadTexture(textures::moon);			
		}
	}
}