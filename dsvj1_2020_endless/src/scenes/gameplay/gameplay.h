#ifndef GAMEPLAY_H
#define GAMEPLAY_H

#include <ctime>
#include <cstdlib>

#include "raylib.h"

#include "functions_library/textures/textures.h"
#include "functions_library/drawings/drawings.h"
#include "functions_library/entities/character.h"
#include "functions_library/entities/entities.h"
#include "functions_library/entities_motion/entities_motion.h"
#include "functions_library/sizes/sizes.h"
#include "functions_library/speed/speed.h"
#include "functions_library/collisions/collisions.h"
#include "functions_library/audio/audio.h"
#include "functions_library/audio/sounds.h"
#include "scenes/game/game.h"

const int maximumShips = 20;
const int maximumStones = 20;
const int maximumStones2 = 20;
const int maximumShots = 5;
const int maximumFlags = 10;
const int maximumCoins = 10;
const int maximumMoon = 10;

namespace endless
{
	namespace gameplay
	{		
		extern int screenWidth;
		extern int screenHeight;

		extern Vector2 mousePoint;
		
		enum class Entitie { SHIP, STONE, STONE2, FLAG, COIN, MOON };
		extern Entitie typeOfEntitie;

		extern const int maxShips;
		extern const int maxStones;
		extern const int maxStones2;
		extern const int maxShots;
		extern const int maxFlags;
		extern const int maxCoins;
		extern const int maxMoon;
		extern int numShip;
		extern int numStone;
		extern int numStone2; 
		extern int numShot;
		extern int numFlag;
		extern int numCoin;
		extern int numMoon;

		extern Character character;
		extern Entities ship[maximumShips];
		extern Entities stone[maximumStones];
		extern Entities stone2[maximumStones2];
		extern Entities shot[maximumShots];
		extern Entities flag[maximumFlags];
		extern Entities coin[maximumCoins];
		extern Entities moon[maximumMoon];

		extern bool pause;

		extern int timer;
		extern float timerCharacter;
		extern int frameCharacter;
		extern float timerFlags;
		extern int frameFlags;
		extern float timerCoins;
		extern int frameCoins;
		extern float timerMoon;
		extern int frameMoon;

		extern int actualScore;
		extern int highScore;

		extern float frameWidth;
		extern int maxFrames;
		extern float frameWidth1;
		extern int maxFrames1;
		extern float frameWidth2;
		extern int maxFrames2;
		extern float frameWidth3;
		extern int maxFrames3;

		extern int cantShots;

		extern int timerSpeed;
		extern int auxTimerSpeed;
		extern float speedModifier;

		extern int periodTimeLowerWay;
		extern int periodTimeMiddleWay;
		extern int periodTimeLastWay;

		extern Sounds sounds;

		extern int timerBackground;

		extern Color audioButtonStatus;

		void init();
		void initialize();
		void update();
		void draw();
		void deinit();
	}
}

#endif