#include "result.h"

using namespace endless;
using namespace gameplay;

namespace endless
{
	namespace result
	{
		static Color playAgainButtonStatus = WHITE;
		static Color menuButtonStatus = WHITE;
		static Color exitButtonStatus = WHITE;

		void init()
		{
			textures::playAgainButton = LoadTexture("res/raw/textures/playagainbutton.png");

			textures::menuButton = LoadTexture("res/raw/textures/menubutton.png");
		}

		void update()
		{
			mousePoint = GetMousePosition();

			if (CheckCollisionPointRec(mousePoint, { pos::resultPosX, pos::resultPosY, (float)textures::playAgainButton.width, (float)textures::playAgainButton.height }))
			{
				playAgainButtonStatus = DARKGRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					game::currentScene = game::Scene::GAMEPLAY;
					initialize();
				}
			}
			else playAgainButtonStatus = WHITE;

			if (CheckCollisionPointRec(mousePoint, { pos::resultPosX2, pos::resultPosY, (float)textures::menuButton.width, (float)textures::menuButton.height }))
			{
				menuButtonStatus = DARKGRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					game::currentScene = game::Scene::MENU;
				}
			}
			else menuButtonStatus = WHITE;

			if (CheckCollisionPointRec(mousePoint, { pos::resultPosX3, pos::resultPosY2, (float)textures::exitButton.width, (float)textures::exitButton.height }))
			{
				exitButtonStatus = DARKGRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					game::exitButton = true;
				}
			}
			else exitButtonStatus = WHITE;

			backgroundMotion();
			StopMusicStream(audio::gameMusic.gameplay);
			audio::menuAudio();
			sounds.button = false;
		}

		void draw()
		{
			drawingBackground();

			DrawTexture(textures::playAgainButton, pos::resultPosX, pos::resultPosY, playAgainButtonStatus);
			DrawTexture(textures::menuButton, pos::resultPosX2, pos::resultPosY, menuButtonStatus);
			DrawTexture(textures::exitButton, pos::resultPosX3, pos::resultPosY2, exitButtonStatus);

			DrawText("Actual score", pos::resultTextPosX, pos::resultTextPosY, sizes::textSize4, LIGHTGRAY);
			DrawText(TextFormat("%06i", actualScore), pos::resultTextPosX2, pos::resultTextPosY2, sizes::textSize3, LIGHTGRAY);

			DrawText("High score", pos::resultTextPosX3, pos::resultTextPosY, sizes::textSize4, LIGHTGRAY);
			DrawText(TextFormat("%06i", highScore), pos::resultTextPosX4, pos::resultTextPosY2, sizes::textSize3, LIGHTGRAY);
		}

		void deinit()
		{
			UnloadTexture(textures::playAgainButton);
		}
	}
}