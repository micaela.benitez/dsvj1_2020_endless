#ifndef RESULT_H
#define RESULT_H

#include "scenes/gameplay/gameplay.h"

namespace endless
{
	namespace result
	{
		void init();
		void update();
		void draw();
		void deinit();
	}
}

#endif