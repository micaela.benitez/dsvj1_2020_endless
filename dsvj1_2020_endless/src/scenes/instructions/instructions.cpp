#include "instructions.h"

using namespace endless;
using namespace gameplay;

namespace endless
{
	namespace instructions
	{
		static Color menuButtonStatus = WHITE;
		static Color rightArrowButtonStatus = WHITE;

		void init()
		{
		}

		void update()
		{
			mousePoint = GetMousePosition();

			if (CheckCollisionPointRec(mousePoint, { pos::upButtons, pos::upButtons, (float)textures::menuButton2.width, (float)textures::menuButton2.height }))
			{
				menuButtonStatus = DARKGRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					game::currentScene = game::Scene::MENU;
				}
			}
			else menuButtonStatus = WHITE;

			if (CheckCollisionPointRec(mousePoint, { pos::creditsUpButton, pos::upButtons, (float)textures::rightArrow.width, (float)textures::rightArrow.height }))
			{
				rightArrowButtonStatus = DARKGRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					game::currentScene = game::Scene::INSTRUCTIONS2;
				}
			}
			else rightArrowButtonStatus = WHITE;

			backgroundMotion();
			audio::menuAudio();
			sounds.button = false;
		}

		void draw()
		{
			drawingBackground();

			DrawText("Instructions", (screenWidth / 2) - MeasureText("Instructions", sizes::textSize5 / 2), pos::instructionsTitlePosY, sizes::textSize5, WHITE);

			DrawText("Welcome to space runner!", (screenWidth / 2) - MeasureText("Welcome to space runner!", sizes::textSize2 / 2), pos::instructionsPosY, sizes::textSize2, WHITE);
			DrawText("In this game you will be an astronaut who", (screenWidth / 2) - MeasureText("In this game you will be an astronaut who", sizes::textSize2 / 2), pos::instructionsPosY2, sizes::textSize2, WHITE);
			DrawText("will have to dodge obstacles, dodging or", (screenWidth / 2) - MeasureText("will have to dodge obstacles, dodging or", sizes::textSize2 / 2), pos::instructionsPosY3, sizes::textSize2, WHITE);
			DrawText("destroying them!", (screenWidth / 2) - MeasureText("destroying them!", sizes::textSize2 / 2), pos::instructionsPosY4, sizes::textSize2, WHITE);
			DrawText("You will also have pickups with different ", (screenWidth / 2) - MeasureText("You will also have pickups with different ", sizes::textSize2 / 2), pos::instructionsPosY5, sizes::textSize2, WHITE);
			DrawText("scores, the more you collect, the more ", (screenWidth / 2) - MeasureText("scores, the more you collect, the more ", sizes::textSize2 / 2), pos::instructionsPosY6, sizes::textSize2, WHITE);
			DrawText("points you will get!", (screenWidth / 2) - MeasureText("points you will get!", sizes::textSize2 / 2), pos::instructionsPosY7, sizes::textSize2, WHITE);

			DrawText("Explore Mercury, Earth and Mars,", (screenWidth / 2) - MeasureText("Explore Mercury, Earth and Mars,", sizes::textSize2 / 2), pos::instructionsPosY8, sizes::textSize2, WHITE);
			DrawText("You only have one life, GOOD LUCK!", (screenWidth / 2) - MeasureText("You only have one life, GOOD LUCK!", sizes::textSize2 / 2), pos::instructionsPosY9, sizes::textSize2, WHITE);

			DrawTexture(textures::menuButton2, pos::upButtons, pos::upButtons, menuButtonStatus);
			DrawTexture(textures::rightArrow, pos::creditsUpButton, pos::upButtons, rightArrowButtonStatus);
		}

		void deinit()
		{
		}
	}
}