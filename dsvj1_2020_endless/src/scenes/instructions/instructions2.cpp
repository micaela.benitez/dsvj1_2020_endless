#include "instructions2.h"

using namespace endless;
using namespace gameplay;

namespace endless
{
	namespace instructions2
	{
		static Color leftArrowButtonStatus = WHITE;
		static Color menuButtonStatus = WHITE;

		void init()
		{
		}

		void update()
		{
			mousePoint = GetMousePosition();

			if (CheckCollisionPointRec(mousePoint, { pos::upButtons, pos::upButtons, (float)textures::rightArrow.width, (float)textures::rightArrow.height }))
			{
				leftArrowButtonStatus = DARKGRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					game::currentScene = game::Scene::INSTRUCTIONS;
				}
			}
			else leftArrowButtonStatus = WHITE;

			if (CheckCollisionPointRec(mousePoint, { pos::credits2UpButton, pos::upButtons, (float)textures::menuButton2.width, (float)textures::menuButton2.height }))
			{
				menuButtonStatus = DARKGRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					game::currentScene = game::Scene::MENU;
				}
			}
			else menuButtonStatus = WHITE;

			backgroundMotion();
			audio::menuAudio();
			framesMotion();
			sounds.button = false;
		}

		void draw()
		{
			drawingBackground();

			DrawText("Instructions", (screenWidth / 2) - MeasureText("Instructions", sizes::textSize5 / 2), pos::instructionsTitlePosY, sizes::textSize5, WHITE);

			DrawText("Pickups", (screenWidth / 2) - MeasureText("Pickups", sizes::textSize2 / 2), pos::instructions2PosY, sizes::textSize2, WHITE);
			DrawTextureRec(textures::flags, { frameWidth1 * frameFlags, 0, frameWidth1, (float)textures::flags.height }, { pos::instructions2PosX, pos::instructions2PosY2 }, WHITE);
			DrawTextureRec(textures::coins, { frameWidth2 * frameCoins, 0, frameWidth2, (float)textures::coins.height }, { pos::instructions2PosX2, pos::instructions2PosY3 }, WHITE);
			DrawTextureRec(textures::moon, { frameWidth3 * frameMoon, 0, frameWidth3, (float)textures::moon.height }, { pos::instructions2PosX3 , pos::instructions2PosY2 }, WHITE);

			DrawText("Obstacles", (screenWidth / 2) - MeasureText("Obstacles", sizes::textSize2 / 2), pos::instructions2PosY4, sizes::textSize2, WHITE);
			DrawTexture(textures::ship, pos::instructions2PosX4, pos::instructions2PosY5, WHITE);
			DrawTexture(textures::stone, pos::instructions2PosX5, pos::instructions2PosY5, WHITE);
			DrawTexture(textures::stone2, pos::instructions2PosX6, pos::instructions2PosY6, WHITE);

			DrawText("For this you", pos::instructions2PosX7, pos::instructions2PosY7, sizes::textSize, WHITE);
			DrawText("have to bend down", pos::instructions2PosX8, pos::instructions2PosY8, sizes::textSize, WHITE);
			DrawText("For this you", (screenWidth / 2) - MeasureText("For this you", sizes::textSize / 2), pos::instructions2PosY7, sizes::textSize, WHITE);
			DrawText("have to jump", (screenWidth / 2) - MeasureText("have to jump", sizes::textSize / 2), pos::instructions2PosY8, sizes::textSize, WHITE);
			DrawText("For this you", pos::instructions2PosX9, pos::instructions2PosY7, sizes::textSize, WHITE);
			DrawText("have to shoot", pos::instructions2PosX10, pos::instructions2PosY8, sizes::textSize, WHITE);

			DrawTexture(textures::leftArrow, pos::upButtons, pos::upButtons, leftArrowButtonStatus);
			DrawTexture(textures::menuButton2, pos::credits2UpButton, pos::upButtons, menuButtonStatus);
		}

		void deinit()
		{
		}
	}
}