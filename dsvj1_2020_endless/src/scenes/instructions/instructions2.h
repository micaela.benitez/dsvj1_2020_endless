#ifndef INSTRUCTIONS2_H
#define INSTRUCTIONS2_H

#include "scenes/gameplay/gameplay.h"

namespace endless
{
	namespace instructions2
	{
		void init();
		void update();
		void draw();
		void deinit();
	}
}

#endif