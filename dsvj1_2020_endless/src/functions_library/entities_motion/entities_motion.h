#ifndef ENTITIES_MOTION_H
#define ENTITIES_MOTION_H

#include "functions_library/positions/positions.h"
#include "scenes/gameplay/gameplay.h"

void framesMotion();
void backgroundMotion();
void characterMotion();
void entitiesMotion();
void speedModifierTimer();

#endif