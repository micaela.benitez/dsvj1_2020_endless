#include "entities_motion.h"

using namespace endless;
using namespace gameplay;

void framesMotion()
{
	// Character
	frameWidth = (float)(textures::character.width / sizes::framesCharacter);
	maxFrames = (int)(textures::character.width / (int)frameWidth);
	timerCharacter += GetFrameTime();
	if (timerCharacter >= speed::speedFramesCharacter)
	{
		timerCharacter = 0.0f;
		frameCharacter++;
	}
	frameCharacter = frameCharacter % maxFrames;

	// Flags
	frameWidth1 = (float)(textures::flags.width / sizes::framesFlag);
	maxFrames1 = (int)(textures::flags.width / (int)frameWidth1);
	timerFlags += GetFrameTime();
	if (timerFlags >= speed::speedFramesEntities2)
	{
		timerFlags = 0.0f;
		frameFlags++;
	}
	frameFlags = frameFlags % maxFrames1;

	// Coins
	frameWidth2 = (float)(textures::coins.width / sizes::framesCoin);
	maxFrames2 = (int)(textures::coins.width / (int)frameWidth2);
	timerCoins += GetFrameTime();
	if (timerCoins >= speed::speedFramesEntities)
	{
		timerCoins = 0.0f;
		frameCoins++;
	}
	frameCoins = frameCoins % maxFrames2;

	// Moon	
	frameWidth3 = (float)(textures::moon.width / sizes::framesMoon);
	maxFrames3 = (int)(textures::moon.width / (int)frameWidth3);
	timerMoon += GetFrameTime();
	if (timerMoon >= speed::speedFramesEntities2)
	{
		timerMoon = 0.0f;
		frameMoon++;
	}
	frameMoon = frameMoon % maxFrames3;
}

void backgroundMotion() 
{
	using namespace pos;
	
	speed::scrollingBackground -= backgroundParallaxPosX * GetFrameTime();
	if (game::currentScene == game::Scene::GAMEPLAY) speed::scrollingBackground2 -= background2ParallaxPosX * GetFrameTime() * speedModifier;
	else speed::scrollingBackground2 -= background3ParallaxPosX * GetFrameTime();

	if (speed::scrollingBackground <= -textures::background.width / 2) speed::scrollingBackground = inicialBackgroundPosX;
	if (game::currentScene == game::Scene::GAMEPLAY) if (speed::scrollingBackground2 <= -textures::background2.width / 2)  speed::scrollingBackground2 = inicialBackgroundPosX;
	else if (speed::scrollingBackground2 <= -textures::backgroundPlanets.width / 2)  speed::scrollingBackground2 = inicialBackgroundPosX;
}

void characterMotion()
{
	using namespace settings;

	if (IsKeyDown(key.right) && !character.jump && !character.bend) character.position.x = (character.position.x + speed::speedCharacter * GetFrameTime() * speedModifier < (screenWidth - textures::character.width / 6)) ? character.position.x += speed::speedCharacter * GetFrameTime() * speedModifier : character.position.x = (screenWidth - textures::character.width / 6);
	else if (IsKeyDown(key.left) && !character.jump && !character.bend) character.position.x = (character.position.x - speed::speedCharacter * GetFrameTime() * speedModifier > 0) ? character.position.x -= speed::speedCharacter * GetFrameTime() * speedModifier : character.position.x = 0;
	else if (!character.changeOfWay && IsKeyDown(key.bend))
	{
		character.bend = true;
		if (character.actualWay == pos::firstWayCharacter) character.position.y = pos::firstWayCharacter;
		else if (character.actualWay == pos::middleWayCharacter) character.position.y = pos::middleWayCharacter;
		else if (character.actualWay == pos::lastWayCharacter) character.position.y = pos::lastWayCharacter;
	}
	else if (!character.changeOfWay && IsKeyDown(key.shoot))
	{
		character.shooter = true;
		if (character.actualWay == pos::firstWayCharacter) character.position.y = pos::firstWayCharacter;
		else if (character.actualWay == pos::middleWayCharacter) character.position.y = pos::middleWayCharacter;
		else if (character.actualWay == pos::lastWayCharacter) character.position.y = pos::lastWayCharacter;

		if (IsKeyPressed(key.shoot))
		{
			if (numShot + 1 < maxShots && !shot[numShot + 1].active)
			{
				sounds.shoot = true;
				numShot++;
				shot[numShot].active = true;
				shot[numShot].scrollingPosX = character.position.x + sizes::characterWidth3;
				shot[numShot].posY = character.position.y + 55;
				
			}
		}
	}
	else if (!character.changeOfWay && IsKeyPressed(key.jump))
	{
		character.jump = true;
		if (character.speedJump < 0) character.speedJump = -character.speedJump;
		if (character.position.y == pos::firstWayCharacter) character.actualWay = pos::firstWayCharacter;
		else if (character.position.y == pos::middleWayCharacter) character.actualWay = pos::middleWayCharacter;
		else if (character.position.y == pos::lastWayCharacter) character.actualWay = pos::lastWayCharacter;
	}
	else if (character.position.y == pos::firstWayCharacter && IsKeyPressed(key.down))
	{
		if (character.speedJump > 0) character.speedJump = -character.speedJump;
		character.actualWay = pos::firstWayCharacter;
		character.nextWay = pos::middleWayCharacter;
		character.changeOfWay = true;
	}
	else if (character.position.y == pos::middleWayCharacter && IsKeyPressed(key.up))
	{
		if (character.speedJump < 0) character.speedJump = -character.speedJump;
		character.actualWay = pos::middleWayCharacter;
		character.nextWay = pos::firstWayCharacter;
		character.changeOfWay = true;
	}
	else if (character.position.y == pos::middleWayCharacter && IsKeyPressed(key.down))
	{
		if (character.speedJump > 0) character.speedJump = -character.speedJump;
		character.actualWay = pos::middleWayCharacter;
		character.nextWay = pos::lastWayCharacter;
		character.changeOfWay = true;
	}
	else if (character.position.y == pos::lastWayCharacter && IsKeyPressed(key.up))
	{
		if (character.speedJump < 0) character.speedJump = -character.speedJump;
		character.actualWay = pos::lastWayCharacter;
		character.nextWay = pos::middleWayCharacter;
		character.changeOfWay = true;
	}
	else
	{
		character.bend = false;
		character.shooter = false;
	}
}

void entitieGenerator(int wayShip, int wayStone, int wayStone2, int wayFlag, int wayCoin, int wayMoon)
{
	static const int quantityObstacles = 6;

	srand(time(NULL));

	typeOfEntitie = (Entitie)(rand() % quantityObstacles);
	if (typeOfEntitie == Entitie::SHIP)
	{
		if (numShip + 1 < maxShips && !ship[numShip + 1].active)
		{
			numShip++;
			ship[numShip].active = true;
			ship[numShip].posY = wayShip;
		}
	}
	else if (typeOfEntitie == Entitie::STONE)
	{
		if (numStone + 1 < maxStones && !stone[numStone + 1].active)
		{
			numStone++;
			stone[numStone].active = true;
			stone[numStone].posY = wayStone;
		}
	}
	else if (typeOfEntitie == Entitie::STONE2)
	{
		if (numStone2 + 1 < maxStones2 && !stone2[numStone2 + 1].active)
		{
			numStone2++;
			stone2[numStone2].active = true;
			stone2[numStone2].posY = wayStone2;
		}
	}
	else if (typeOfEntitie == Entitie::FLAG)
	{
		if (numFlag + 1 < maxFlags && !flag[numFlag + 1].active)
		{
			numFlag++;
			flag[numFlag].active = true;
			flag[numFlag].posY = wayFlag;
		}
	}
	else if (typeOfEntitie == Entitie::COIN)
	{
		if (numCoin + 1 < maxCoins && !coin[numCoin + 1].active)
		{
			numCoin++;
			coin[numCoin].active = true;
			coin[numCoin].posY = wayCoin;
		}
	}
	else 
	{
		if (numMoon + 1 < maxMoon && !moon[numMoon + 1].active)
		{
			numMoon++;
			moon[numMoon].active = true;
			moon[numMoon].posY = wayMoon;
		}
	}
}

void entitiesMotion()
{
	using namespace pos;

	if (speedModifier == 1.0f)
	{
		periodTimeLowerWay = 150;
		periodTimeMiddleWay = 250;
		periodTimeLastWay = 200;
	}
	else if (speedModifier == 1.5f)
	{
		periodTimeLowerWay = 100;
		periodTimeMiddleWay = 166;
		periodTimeLastWay = 133;
	}
	else if (speedModifier == 2.0)
	{
		periodTimeLowerWay = 75;
		periodTimeMiddleWay = 125;
		periodTimeLastWay = 100;
	}

	if (numShip == (maxShips - 1)) numShip = -1;
	if (numStone == (maxStones - 1)) numStone = -1;
	if (numStone2 == (maxStones2 - 1)) numStone2 = -1;
	if (numFlag == (maxFlags - 1)) numFlag = -1;
	if (numCoin == (maxCoins - 1)) numCoin = -1;
	if (numMoon == (maxMoon - 1)) numMoon = -1;

	timer += 100 * GetFrameTime();

	if (timer % periodTimeLowerWay == 0) entitieGenerator(firstWayShip, firstWayStone, firstWayStone2, firstWayFlag, firstWayCoin, firstWayMoon);   // First way
	else if (timer % periodTimeMiddleWay == 0 || timer == 100) entitieGenerator(middleWayShip, middleWayStone, middleWayStone2, middleWayFlag, middleWayCoin, middleWayMoon);   // Middle way
	else if (timer % periodTimeLastWay == 0)entitieGenerator(lastWayShip, lastWayStone, lastWayStone2, lastWayFlag, lastWayCoin, lastWayMoon);   // Last way
}

void speedModifierTimer()
{
	timerSpeed++;
	if (speedModifier < 2.0f)
	{
		if (((timerSpeed / 1000) % 4 == 0) && (timerSpeed / 1000 != auxTimerSpeed)) speedModifier += 0.5f;
		if ((timerSpeed / 1000) % 4 == 0) auxTimerSpeed = timerSpeed / 1000;
	}
}

void backgroundModifierTimer()
{
	timerBackground++;
	if (timerBackground / 1000 == 12) timerBackground = 1000;
}