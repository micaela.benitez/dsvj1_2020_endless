#ifndef SPEED_H
#define SPEED_H

#include "scenes/gameplay/gameplay.h"

namespace speed
{
	extern float scrollingBackground;
	extern float scrollingBackground2;
	extern float speedFramesCharacter;
	extern float speedFramesEntities;
	extern float speedFramesEntities2;
	extern float speedCharacter;
	extern float speedJumpCharacter;
	extern float entitiesParallaxPosX;
	extern float entitiesParallaxPosX2;
}

#endif