#include "textures.h"

namespace textures
{
	Texture2D background;
	Texture2D background2;
	Texture2D background3;
	Texture2D background4;
	Texture2D backgroundPlanets;
	Texture2D stone;
	Texture2D stone2;
	Texture2D ship;
	Texture2D character;
	Texture2D characterJump;
	Texture2D characterBend;
	Texture2D characterShooter;
	Texture2D characterNormal;
	Texture2D shots;
	Texture2D highScoreMenu;
	Texture2D flags;
	Texture2D coins;
	Texture2D moon;

	// Buttons
	Texture2D startButton;
	Texture2D instructionsButton;
	Texture2D settingsButton;
	Texture2D creditsButton;
	Texture2D exitButton;
	Texture2D playAgainButton;
	Texture2D menuButton;
	Texture2D menuButton2;
	Texture2D exitButton2;
	Texture2D rightArrow;
	Texture2D leftArrow;
	Texture2D defaultControls;
	Texture2D audioButton;
	Texture2D resolutionButton;
	Texture2D quitButton;
}