#ifndef TEXTURES_H
#define TEXTURES_H

#include "raylib.h"

namespace textures
{
	extern Texture2D background;
	extern Texture2D background2;
	extern Texture2D background3;
	extern Texture2D background4;
	extern Texture2D backgroundPlanets;
	extern Texture2D stone;
	extern Texture2D stone2;
	extern Texture2D ship;
	extern Texture2D character;
	extern Texture2D characterJump;
	extern Texture2D characterBend;
	extern Texture2D characterShooter;
	extern Texture2D characterNormal;
	extern Texture2D shots;
	extern Texture2D highScoreMenu;
	extern Texture2D flags;
	extern Texture2D coins;
	extern Texture2D moon;

	// Buttons
	extern Texture2D startButton;
	extern Texture2D instructionsButton;
	extern Texture2D settingsButton;
	extern Texture2D creditsButton;
	extern Texture2D exitButton;
	extern Texture2D playAgainButton;
	extern Texture2D menuButton;
	extern Texture2D menuButton2;
	extern Texture2D exitButton2;
	extern Texture2D rightArrow;
	extern Texture2D leftArrow;
	extern Texture2D defaultControls;
	extern Texture2D audioButton;
	extern Texture2D resolutionButton;
	extern Texture2D quitButton;
}

#endif