#ifndef DRAWINGS_H
#define DRAWINGS_H

#include "functions_library/positions/positions.h"
#include "scenes/gameplay/gameplay.h"

void drawingBackground();
void drawingCharacter();
void drawingObstacles();
void drawingPickups();
void drawingShots();
void drawingScreenDetails();
void backgroundModifierTimer();

#endif
