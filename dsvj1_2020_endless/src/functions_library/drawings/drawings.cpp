#include "drawings.h"

using namespace endless;
using namespace gameplay;

void drawingBackground()
{
	using namespace pos;
	
	DrawTexture(textures::background, speed::scrollingBackground, backgroundPosY, WHITE);
	DrawTexture(textures::background, textures::background.width * 2 + speed::scrollingBackground, backgroundPosY, WHITE);

	if (game::currentScene == game::Scene::GAMEPLAY)
	{
		if (timerBackground / 1000 >= 4 && timerBackground / 1000 < 8)
		{
			DrawTexture(textures::background3, speed::scrollingBackground2, backgroundPosY, WHITE);
			DrawTexture(textures::background3, textures::background2.width * 2 + speed::scrollingBackground2, backgroundPosY, WHITE);
		}
		else if(timerBackground / 1000 >= 8 && timerBackground / 1000 < 12)
		{
			DrawTexture(textures::background4, speed::scrollingBackground2, backgroundPosY, WHITE);
			DrawTexture(textures::background4, textures::background2.width * 2 + speed::scrollingBackground2, backgroundPosY, WHITE);
		}
		else
		{
			DrawTexture(textures::background2, speed::scrollingBackground2, backgroundPosY, WHITE);
			DrawTexture(textures::background2, textures::background2.width * 2 + speed::scrollingBackground2, backgroundPosY, WHITE);
		}
		#if DEBUG
		DrawLine(0, pos::firstWayCharacter + sizes::characterHeight, screenWidth, pos::firstWayCharacter + sizes::characterHeight, WHITE);
		DrawLine(0, pos::middleWayCharacter + sizes::characterHeight, screenWidth, pos::middleWayCharacter + sizes::characterHeight, WHITE);
		DrawLine(0, pos::lastWayCharacter + sizes::characterHeight, screenWidth, pos::lastWayCharacter + sizes::characterHeight, WHITE);
		#endif
	}
	else
	{
		DrawTexture(textures::backgroundPlanets, speed::scrollingBackground2, backgroundPosY, WHITE);
		DrawTexture(textures::backgroundPlanets, textures::backgroundPlanets.width * 2 + speed::scrollingBackground2, backgroundPosY, WHITE);
	}	
}

void drawingCharacter()
{
	using namespace pos;

	if (character.bend)
	{
		DrawTexture(textures::characterBend, character.position.x, character.position.y, WHITE);
		#if DEBUG
		DrawRectangleLines(character.position.x, character.position.y, textures::characterBend.width, textures::characterBend.height, DARKGRAY);
		DrawRectangleLines(character.position.x + collisionAreaBendPosX, character.position.y + collisionAreaBendPosY, textures::characterBend.width - collisionAreaBendWidth, textures::characterBend.height - collisionAreaBendHeight, WHITE);
		#endif
		if (character.jump) character.jump = false;
	}
	else if (character.shooter)
	{
		DrawTexture(textures::characterShooter, character.position.x, character.position.y, WHITE);
		#if DEBUG
		DrawRectangleLines(character.position.x, character.position.y, textures::characterShooter.width, textures::characterShooter.height, DARKGRAY);
		DrawRectangleLines(character.position.x + collisionAreShooter, character.position.y + collisionAreShooter, textures::characterShooter.width - collisionAreShooter, textures::characterShooter.height - collisionAreShooter, WHITE);
		#endif
		if (character.jump) character.jump = false;		
	}
	else if (character.jump)
	{
		character.position.y -= character.speedJump * GetFrameTime() * speedModifier;
		if (character.position.y <= character.actualWay - pos::maxPosYJump) character.speedJump = -character.speedJump;
		else if (character.position.y >= character.actualWay)
		{
			sounds.jump = true;
			character.jump = false;
			character.position.y = character.actualWay;
		}
		DrawTexture(textures::characterJump, character.position.x, character.position.y, WHITE);	
		#if DEBUG
		DrawRectangleLines(character.position.x, character.position.y, textures::characterJump.width, textures::characterJump.height, DARKGRAY);
		DrawRectangleLines(character.position.x + collisionAreaJumpPosX, character.position.y + collisionAreaJumpPosY, textures::characterJump.width - collisionAreaJumpWidth, textures::characterJump.height - collisionAreaJumpHeight, WHITE);
		#endif
	}
	else if (character.changeOfWay)
	{
		character.position.y -= character.speedJump*4 * GetFrameTime() * speedModifier;
		if (character.nextWay > character.actualWay && character.position.y >= character.nextWay)
		{
			sounds.jump = true;
			character.changeOfWay = false;
			character.position.y = character.nextWay;
			character.actualWay = character.nextWay;
		}
		else if (character.nextWay < character.actualWay && character.position.y <= character.nextWay)
		{
			sounds.jump = true;
			character.changeOfWay = false;
			character.position.y = character.nextWay;
			character.actualWay = character.nextWay;
		}
		DrawTexture(textures::characterJump, character.position.x, character.position.y, WHITE);
		#if DEBUG
		DrawRectangleLines(character.position.x, character.position.y, textures::characterJump.width, textures::characterJump.height, DARKGRAY);
		DrawRectangleLines(character.position.x + collisionAreaJumpPosX, character.position.y + collisionAreaJumpPosY, textures::characterJump.width - collisionAreaJumpWidth, textures::characterJump.height - collisionAreaJumpHeight, WHITE);
		#endif
	}
	else if (timer == 0 && !IsKeyDown(settings::key.right) && !IsKeyDown(settings::key.left))
	{
		DrawTexture(textures::characterNormal, character.position.x, character.position.y, WHITE);
	}
	else 
	{
		DrawTextureRec(textures::character, { frameWidth * frameCharacter, 0, frameWidth, (float)textures::character.height }, { character.position.x, character.position.y }, WHITE);
		#if DEBUG
		DrawRectangleLines(character.position.x, character.position.y, (textures::character.width / sizes::framesCharacter), textures::character.height, DARKGRAY);
		DrawRectangleLines(character.position.x + collisionAreaNormalPosX, character.position.y, (textures::character.width / sizes::framesCharacter) - collisionAreaNormalWidth, textures::character.height, WHITE);
		#endif
	}	
}

void drawingObstacles()
{
	using namespace pos;

	for (int i = 0; i < maxShips; i++)
	{
		if (ship[i].active)
		{
			DrawTexture(ship[i].texture, ship[i].scrollingPosX, ship[i].posY, WHITE);
			#if DEBUG
			DrawRectangleLines(ship[i].scrollingPosX, ship[i].posY, textures::ship.width, textures::ship.height, DARKGRAY);
			DrawRectangleLines(ship[i].scrollingPosX, ship[i].posY + collisionAreaShipPosY, textures::ship.width, textures::ship.height - collisionAreaShipHeight, WHITE);
			#endif
			if (character.active && !pause) ship[i].scrollingPosX -= speed::entitiesParallaxPosX * GetFrameTime() * speedModifier;
			if (ship[i].scrollingPosX < -textures::ship.width) ship[i].active = false;
		}
		else ship[i].scrollingPosX = screenWidth;
	}

	for (int i = 0; i < maxStones; i++)
	{
		if (stone[i].active)
		{
			DrawTexture(stone[i].texture, stone[i].scrollingPosX, stone[i].posY, WHITE);
			#if DEBUG			
			DrawRectangleLines(stone[i].scrollingPosX, stone[i].posY, textures::stone.width, textures::stone.height, DARKGRAY);
			DrawRectangleLines(stone[i].scrollingPosX + collisionAreaStonePosX, stone[i].posY + collisionAreaStonePosY, textures::stone.width - collisionAreaStoneWidth, textures::stone.height - collisionAreaStoneHeight, WHITE);
			#endif
			if (character.active && !pause) stone[i].scrollingPosX -= speed::entitiesParallaxPosX2 * GetFrameTime() * speedModifier;
			if (stone[i].scrollingPosX < -textures::stone.width) stone[i].active = false;
		}
		else stone[i].scrollingPosX = screenWidth;
	}

	for (int i = 0; i < maxStones2; i++)
	{
		if (stone2[i].active)
		{
			DrawTexture(stone2[i].texture, stone2[i].scrollingPosX, stone2[i].posY, WHITE);
			#if DEBUG			
			DrawRectangleLines(stone2[i].scrollingPosX, stone2[i].posY, textures::stone2.width, textures::stone2.height, DARKGRAY);
			DrawRectangleLines(stone2[i].scrollingPosX + collisionAreaStone2PosX, stone2[i].posY, textures::stone2.width - collisionAreaStone2Width, textures::stone2.height, WHITE);
			#endif
			if (character.active && !pause) stone2[i].scrollingPosX -= speed::entitiesParallaxPosX2 * GetFrameTime() * speedModifier;
			if (stone2[i].scrollingPosX < -textures::stone2.width) stone2[i].active = false;
		}
		else stone2[i].scrollingPosX = screenWidth;
	}
}

void drawingPickups()
{
	using namespace pos;

	for (int i = 0; i < maxFlags; i++)
	{
		if (flag[i].active)
		{
			DrawTextureRec(flag[i].texture, { frameWidth1 * frameFlags, 0, frameWidth1, (float)textures::flags.height }, { flag[i].scrollingPosX, flag[i].posY }, WHITE);
			#if DEBUG
			DrawRectangleLines(flag[i].scrollingPosX, flag[i].posY, textures::flags.width / sizes::framesFlag, textures::flags.height, WHITE);
			#endif
			if (character.active && !pause) flag[i].scrollingPosX -= speed::entitiesParallaxPosX2 * GetFrameTime() * speedModifier;
			if (flag[i].scrollingPosX < -textures::flags.width) flag[i].active = false;
		}
		else flag[i].scrollingPosX = screenWidth;
	}

	for (int i = 0; i < maxCoins; i++)
	{
		if (coin[i].active)
		{
			DrawTextureRec(coin[i].texture, { frameWidth2 * frameCoins, 0, frameWidth2, (float)textures::coins.height }, { coin[i].scrollingPosX, coin[i].posY }, WHITE);
			#if DEBUG
			DrawRectangleLines(coin[i].scrollingPosX, coin[i].posY, textures::coins.width / sizes::framesCoin, textures::coins.height, WHITE);
			#endif
			if (character.active && !pause) coin[i].scrollingPosX -= speed::entitiesParallaxPosX2 * GetFrameTime() * speedModifier;
			if (coin[i].scrollingPosX < -textures::coins.width) coin[i].active = false;
		}
		else coin[i].scrollingPosX = screenWidth;
	}

	for (int i = 0; i < maxMoon; i++)
	{
		if (moon[i].active)
		{
			DrawTextureRec(moon[i].texture, { frameWidth3 * frameMoon, 0, frameWidth3, (float)textures::moon.height }, { moon[i].scrollingPosX, moon[i].posY }, WHITE);
			#if DEBUG
			DrawRectangleLines(moon[i].scrollingPosX, moon[i].posY, textures::moon.width / sizes::framesMoon, textures::moon.height, WHITE);
			#endif
			if (character.active && !pause) moon[i].scrollingPosX -= speed::entitiesParallaxPosX2 * GetFrameTime() * speedModifier;
			if (moon[i].scrollingPosX < -textures::moon.width) moon[i].active = false;
		}
		else moon[i].scrollingPosX = screenWidth;
	}
}

void drawingShots()
{
	for (int i = 0; i < maxShots; i++)
	{
		if (shot[i].active)
		{
			DrawTexture(textures::shots, shot[i].scrollingPosX, shot[i].posY, WHITE);
			shot[i].scrollingPosX += speed::entitiesParallaxPosX * GetFrameTime() * speedModifier;
			if (shot[i].scrollingPosX > screenWidth) shot[i].active = false;
			#if DEBUG
			DrawRectangleLines(shot[i].scrollingPosX, shot[i].posY, textures::shots.width, textures::shots.height, WHITE);
			#endif
		}
		if (numShot == (maxShots - 1)) numShot = -1;
	}
}

void drawingScreenDetails()
{
	cantShots = 0;

	for (int i = 0; i < maxShots; i++) if (!shot[i].active) cantShots++;
	DrawText(TextFormat("Bullets: %i", cantShots), 20, screenHeight - 40, sizes::textSize, BLACK);

	DrawText("Score", pos::scorePosx, pos::scorePosY, sizes::textSize, LIGHTGRAY);
	DrawText(TextFormat("%06i", actualScore), pos::scorePosX2, pos::scorePosY, sizes::textSize, LIGHTGRAY);

	DrawText("High score", pos::highScorePosx, pos::highScorePosY, sizes::textSize, LIGHTGRAY);
	DrawText(TextFormat("%06i", highScore), pos::highScorePosX2, pos::highScorePosY, sizes::textSize, LIGHTGRAY);

	if (pause)
	{
		if (timer == 0)
		{
			DrawText("Press [P] to start", (screenWidth / 2) - MeasureText("Press [P] to start", sizes::textSize2 / 2), pos::tutorialPosY, sizes::textSize2, WHITE);
			DrawText("To see the obstacles and pickups go to the instructions", (screenWidth / 2) - MeasureText("To see the obstacles and pickups go to the instructions      ", sizes::textSize / 2), pos::tutorialPosY2, sizes::textSize, WHITE);
			drawKeys();
		}
		else
		{
			DrawText("Pause [P]", (screenWidth / 2) - MeasureText("Pause [P]", sizes::textSize5 / 2), screenHeight / 2, sizes::textSize5, WHITE);
			DrawTexture(textures::audioButton, pos::gameplayCenterButton, pos::gameplayDownButton, audioButtonStatus);
		}
	}	
}