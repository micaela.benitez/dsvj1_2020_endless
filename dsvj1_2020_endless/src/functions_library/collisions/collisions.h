#ifndef COLLISIONS_H
#define COLLISIONS_H

#include "scenes/gameplay/gameplay.h"

void collisionShip();
void collisionStone();
void collisionStone2();
void collisionFlag();
void collisionCoin();
void collisionMoon();

#endif
