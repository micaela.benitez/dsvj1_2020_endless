#include "collisions.h"

using namespace endless;
using namespace gameplay;

void loserCondition()
{
	sounds.loser = true;
	character.active = false;
	game::currentScene = game::Scene::RESULT;
}

void collisionShip()
{
	using namespace pos;

	for (int i = 0; i < maxShips; i++)
	{
		if (ship[i].active)
		{
			if (character.jump || character.changeOfWay)
			{
				if (CheckCollisionRecs({ character.position.x + collisionAreaJumpPosX, character.position.y + collisionAreaJumpPosY, (float)textures::characterJump.width - collisionAreaJumpWidth, (float)textures::characterJump.height - collisionAreaJumpHeight }, { ship[i].scrollingPosX, ship[i].posY + collisionAreaShipPosY, (float)textures::ship.width, (float)textures::ship.height - collisionAreaShipHeight })) loserCondition();
			}
			else if (character.bend)
			{
				if (CheckCollisionRecs({ character.position.x + collisionAreaBendPosX, character.position.y + collisionAreaBendPosY, (float)textures::characterBend.width - collisionAreaBendWidth, (float)textures::characterBend.height - collisionAreaBendHeight }, { ship[i].scrollingPosX, ship[i].posY + collisionAreaShipPosY, (float)textures::ship.width, (float)textures::ship.height - collisionAreaShipHeight })) loserCondition();
			}
			else if (character.shooter)
			{
				if (CheckCollisionRecs({ character.position.x + collisionAreShooter, character.position.y + collisionAreShooter, (float)textures::characterShooter.width - collisionAreShooter, (float)textures::characterShooter.height - collisionAreShooter }, { ship[i].scrollingPosX, ship[i].posY + collisionAreaShipPosY, (float)textures::ship.width, (float)textures::ship.height - collisionAreaShipHeight })) loserCondition();
			}
			else
			{
				if (CheckCollisionRecs({ character.position.x + collisionAreaNormalPosX, character.position.y, (float)((textures::character.width / sizes::framesCharacter) - collisionAreaNormalWidth), (float)textures::character.height }, { ship[i].scrollingPosX, ship[i].posY + collisionAreaShipPosY, (float)textures::ship.width, (float)textures::ship.height - collisionAreaShipHeight })) loserCondition();
			}
		}
	}
}

void collisionStone()
{
	using namespace pos;

	for (int i = 0; i < maxStones; i++)
	{
		if (stone[i].active)
		{
			if (character.jump || character.changeOfWay)
			{
				if (CheckCollisionRecs({ character.position.x + collisionAreaJumpPosX, character.position.y + collisionAreaJumpPosY, (float)textures::characterJump.width - collisionAreaJumpWidth, (float)textures::characterJump.height - collisionAreaJumpHeight }, { stone[i].scrollingPosX + collisionAreaStonePosX, stone[i].posY + collisionAreaStonePosY, (float)textures::stone.width - collisionAreaStoneWidth, (float)textures::stone.height - collisionAreaStoneHeight })) loserCondition();
			}
			else if (character.bend)
			{
				if (CheckCollisionRecs({ character.position.x + collisionAreaBendPosX, character.position.y + collisionAreaBendPosY, (float)textures::characterBend.width - collisionAreaBendWidth, (float)textures::characterBend.height - collisionAreaBendHeight }, { stone[i].scrollingPosX + collisionAreaStonePosX, stone[i].posY + collisionAreaStonePosY, (float)textures::stone.width - collisionAreaStoneWidth, (float)textures::stone.height - collisionAreaStoneHeight })) loserCondition();
			}
			else if (character.shooter)
			{
				if (CheckCollisionRecs({ character.position.x + collisionAreShooter, character.position.y + collisionAreShooter, (float)textures::characterShooter.width - collisionAreShooter, (float)textures::characterShooter.height - collisionAreShooter }, { stone[i].scrollingPosX + collisionAreaStonePosX, stone[i].posY + collisionAreaStonePosY, (float)textures::stone.width - collisionAreaStoneWidth, (float)textures::stone.height - collisionAreaStoneHeight })) loserCondition();
			}
			else
			{
				if (CheckCollisionRecs({ character.position.x + collisionAreaNormalPosX, character.position.y, (float)((textures::character.width / sizes::framesCharacter) - collisionAreaNormalWidth), (float)textures::character.height }, { stone[i].scrollingPosX + collisionAreaStonePosX, stone[i].posY + collisionAreaStonePosY, (float)textures::stone.width - collisionAreaStoneWidth, (float)textures::stone.height - collisionAreaStoneHeight })) loserCondition();
			}			
		}
	}
}

void collisionStone2()
{
	using namespace pos;

	for (int i = 0; i < maxShots; i++)
	{
		for (int j = 0; j < maximumStones2; j++)
		{
			if (stone2[j].active)
			{
				if (character.jump || character.changeOfWay)
				{
					if (CheckCollisionRecs({ character.position.x + collisionAreaJumpPosX, character.position.y + collisionAreaJumpPosY, (float)textures::characterJump.width - collisionAreaJumpWidth, (float)textures::characterJump.height - collisionAreaJumpHeight }, { stone2[j].scrollingPosX, stone2[j].posY, (float)textures::stone2.width, (float)textures::stone2.height })) loserCondition();
				}
				else if (character.bend)
				{
					if (CheckCollisionRecs({ character.position.x + collisionAreaBendPosX, character.position.y + collisionAreaBendPosY, (float)textures::characterBend.width - collisionAreaBendWidth, (float)textures::characterBend.height - collisionAreaBendHeight }, { stone2[j].scrollingPosX, stone2[j].posY, (float)textures::stone2.width, (float)textures::stone2.height })) loserCondition();
				}
				else if (character.shooter)
				{
					if (CheckCollisionRecs({ character.position.x + collisionAreShooter, character.position.y + collisionAreShooter, (float)textures::characterShooter.width - collisionAreShooter, (float)textures::characterShooter.height - collisionAreShooter }, { stone2[j].scrollingPosX, stone2[j].posY, (float)textures::stone2.width, (float)textures::stone2.height })) loserCondition();
				}
				else
				{
					if (CheckCollisionRecs({ character.position.x + collisionAreaNormalPosX, character.position.y, (float)((textures::character.width / sizes::framesCharacter) - collisionAreaNormalWidth), (float)textures::character.height }, { stone2[j].scrollingPosX, stone2[j].posY, (float)textures::stone2.width, (float)textures::stone2.height })) loserCondition();
				}

				if (shot[i].active)
				{
					if (CheckCollisionRecs({ shot[i].scrollingPosX, shot[i].posY, (float)textures::shots.width, (float)textures::shots.height }, { stone2[j].scrollingPosX + collisionAreaStone2PosX, stone2[j].posY, (float)textures::stone2.width - collisionAreaStone2Width, (float)textures::stone2.height }))
					{
						sounds.stone_broken = true;
						stone2[j].active = false;
						shot[i].active = false;
					}
				}
			}
		}
	}
}

void collisionFlag()
{
	using namespace pos;

	for (int i = 0; i < maxFlags; i++)
	{
		if (flag[i].active)
		{
			if (character.jump || character.changeOfWay)
			{
				if (CheckCollisionRecs({ character.position.x + collisionAreaJumpPosX, character.position.y + collisionAreaJumpPosY, (float)textures::characterJump.width - collisionAreaJumpWidth, (float)textures::characterJump.height - collisionAreaJumpHeight }, { flag[i].scrollingPosX, flag[i].posY, (float)textures::flags.width / sizes::framesFlag, (float)textures::flags.height }))
				{
					sounds.pickup = true;
					flag[i].active = false;
					actualScore += 10;
				}
			}
			else if (character.bend)
			{
				if (CheckCollisionRecs({ character.position.x + collisionAreaBendPosX, character.position.y + collisionAreaBendPosY, (float)textures::characterBend.width - collisionAreaBendWidth, (float)textures::characterBend.height - collisionAreaBendHeight }, { flag[i].scrollingPosX, flag[i].posY, (float)textures::flags.width / sizes::framesFlag, (float)textures::flags.height }))
				{
					sounds.pickup = true;
					flag[i].active = false;
					actualScore += 10;
				}
			}
			else if (character.shooter)
			{
				if (CheckCollisionRecs({ character.position.x + collisionAreShooter, character.position.y + collisionAreShooter, (float)textures::characterShooter.width - collisionAreShooter, (float)textures::characterShooter.height - collisionAreShooter }, { flag[i].scrollingPosX, flag[i].posY, (float)textures::flags.width / sizes::framesFlag, (float)textures::flags.height }))
				{
					sounds.pickup = true;
					flag[i].active = false;
					actualScore += 10;
				}
			}
			else
			{
				if (CheckCollisionRecs({ character.position.x + collisionAreaNormalPosX, character.position.y, (float)((textures::character.width / sizes::framesCharacter) - collisionAreaNormalWidth), (float)textures::character.height }, { flag[i].scrollingPosX, flag[i].posY, (float)textures::flags.width / sizes::framesFlag, (float)textures::flags.height }))
				{
					sounds.pickup = true;
					flag[i].active = false;
					actualScore += 10;
				}
			}
		}
	}
}

void collisionCoin()
{
	using namespace pos;

	for (int i = 0; i < maxCoins; i++)
	{
		if (coin[i].active)
		{
			if (character.jump || character.changeOfWay)
			{
				if (CheckCollisionRecs({ character.position.x + collisionAreaJumpPosX, character.position.y + collisionAreaJumpPosY, (float)textures::characterJump.width - collisionAreaJumpWidth, (float)textures::characterJump.height - collisionAreaJumpHeight }, { coin[i].scrollingPosX, coin[i].posY, (float)textures::coins.width / sizes::framesCoin, (float)textures::coins.height }))
				{
					sounds.pickup = true;
					coin[i].active = false;
					actualScore += 5;
				}
			}
			else if (character.bend)
			{
				if (CheckCollisionRecs({ character.position.x + collisionAreaBendPosX, character.position.y + collisionAreaBendPosY, (float)textures::characterBend.width - collisionAreaBendWidth, (float)textures::characterBend.height - collisionAreaBendHeight }, { coin[i].scrollingPosX, coin[i].posY, (float)textures::coins.width / sizes::framesCoin, (float)textures::coins.height }))
				{
					sounds.pickup = true;
					coin[i].active = false;
					actualScore += 5;
				}
			}
			else if (character.shooter)
			{
				if (CheckCollisionRecs({ character.position.x + collisionAreShooter, character.position.y + collisionAreShooter, (float)textures::characterShooter.width - collisionAreShooter, (float)textures::characterShooter.height - collisionAreShooter }, { coin[i].scrollingPosX, coin[i].posY, (float)textures::coins.width / sizes::framesCoin, (float)textures::coins.height }))
				{
					sounds.pickup = true;
					coin[i].active = false;
					actualScore += 5;
				}
			}
			else
			{
				if (CheckCollisionRecs({ character.position.x + collisionAreaNormalPosX, character.position.y, (float)((textures::character.width / sizes::framesCharacter) - collisionAreaNormalWidth), (float)textures::character.height }, { coin[i].scrollingPosX, coin[i].posY, (float)textures::coins.width / sizes::framesCoin, (float)textures::coins.height }))
				{
					sounds.pickup = true;
					coin[i].active = false;
					actualScore += 5;
				}
			}
		}
	}
}

void collisionMoon()
{
	using namespace pos;

	for (int i = 0; i < maxMoon; i++)
	{
		if (moon[i].active)
		{
			if (character.jump || character.changeOfWay)
			{
				if (CheckCollisionRecs({ character.position.x + collisionAreaJumpPosX, character.position.y + collisionAreaJumpPosY, (float)textures::characterJump.width - collisionAreaJumpWidth, (float)textures::characterJump.height - collisionAreaJumpHeight }, { moon[i].scrollingPosX, moon[i].posY, (float)textures::moon.width / sizes::framesMoon, (float)textures::moon.height }))
				{
					sounds.pickup = true;
					moon[i].active = false;
					actualScore += 20;
				}
			}
			else if (character.bend)
			{
				if (CheckCollisionRecs({ character.position.x + collisionAreaBendPosX, character.position.y + collisionAreaBendPosY, (float)textures::characterBend.width - collisionAreaBendWidth, (float)textures::characterBend.height - collisionAreaBendHeight }, { moon[i].scrollingPosX, moon[i].posY, (float)textures::moon.width / sizes::framesMoon, (float)textures::moon.height }))
				{
					sounds.pickup = true;
					moon[i].active = false;
					actualScore += 20;
				}
			}
			else if (character.shooter)
			{
				if (CheckCollisionRecs({ character.position.x + collisionAreShooter, character.position.y + collisionAreShooter, (float)textures::characterShooter.width - collisionAreShooter, (float)textures::characterShooter.height - collisionAreShooter }, { moon[i].scrollingPosX, moon[i].posY, (float)textures::moon.width / sizes::framesMoon, (float)textures::moon.height }))
				{
					sounds.pickup = true;
					moon[i].active = false;
					actualScore += 20;
				}
			}
			else
			{
				if (CheckCollisionRecs({ character.position.x + collisionAreaNormalPosX, character.position.y, (float)((textures::character.width / sizes::framesCharacter) - collisionAreaNormalWidth), (float)textures::character.height }, { moon[i].scrollingPosX, moon[i].posY, (float)textures::moon.width / sizes::framesMoon, (float)textures::moon.height }))
				{
					sounds.pickup = true;
					moon[i].active = false;
					actualScore += 20;
				}
			}
		}
	}
}