#ifndef AUDIO_H 
#define AUDIO_H

#include "raylib.h"

#include "scenes/gameplay/gameplay.h"

namespace audio
{
	struct GameSounds
	{
		Sound button;
		Sound jump;
		Sound pickup;
		Sound shoot;
		Sound stone_broken;
		Sound loser;
	};

	struct GameMusic
	{
		Music menu;
		Music gameplay;
	};

	extern GameMusic gameMusic;
	extern float musicVolume;

	extern GameSounds gameSounds;
	extern float soundVolume;

	extern void menuAudio();
	extern void gameplayAudio();

	extern void loadSounds();
	extern void setSoundsVolume();
	extern void unloadSounds();

	extern void loadMusic();
	extern void setMusicVolume();
	extern void unloadMusic();
}

#endif