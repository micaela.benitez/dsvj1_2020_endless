#include "audio.h"

using namespace endless;
using namespace gameplay;

namespace audio
{
	GameMusic gameMusic;
	float musicVolume = 0.4f;

	GameSounds gameSounds;
	float soundVolume = 5.0f;

	void menuAudio()
	{
		UpdateMusicStream(gameMusic.menu);

		if (sounds.button) PlaySoundMulti(gameSounds.button);
	}

	void gameplayAudio()
	{
		UpdateMusicStream(gameMusic.gameplay);

		if (sounds.button) PlaySoundMulti(gameSounds.button);
		if (sounds.jump) PlaySoundMulti(gameSounds.jump);
		if (sounds.pickup) PlaySoundMulti(gameSounds.pickup);
		if (sounds.shoot) PlaySoundMulti(gameSounds.shoot);
		if (sounds.stone_broken) PlaySoundMulti(gameSounds.stone_broken);
		if (sounds.loser) PlaySoundMulti(gameSounds.loser);
	}

	void loadSounds()
	{
		gameSounds.button = LoadSound("res/assets/audio/sounds/button.wav");
		gameSounds.jump = LoadSound("res/assets/audio/sounds/jump.wav");
		gameSounds.pickup = LoadSound("res/assets/audio/sounds/pickup.wav");
		gameSounds.shoot = LoadSound("res/assets/audio/sounds/shoot.wav");
		gameSounds.stone_broken = LoadSound("res/assets/audio/sounds/stonebroken.wav");
		gameSounds.loser = LoadSound("res/assets/audio/sounds/loser.mp3");
	}

	void setSoundsVolume()
	{
		SetSoundVolume(gameSounds.button, soundVolume);
		SetSoundVolume(gameSounds.jump, soundVolume);
		SetSoundVolume(gameSounds.pickup, soundVolume);
		SetSoundVolume(gameSounds.shoot, soundVolume);
		SetSoundVolume(gameSounds.stone_broken, soundVolume);
		SetSoundVolume(gameSounds.loser, soundVolume);
	}

	void unloadSounds()
	{
		UnloadSound(gameSounds.button);
		UnloadSound(gameSounds.jump);
		UnloadSound(gameSounds.pickup);
		UnloadSound(gameSounds.shoot);
		UnloadSound(gameSounds.stone_broken);
		UnloadSound(gameSounds.loser);
	}

	void loadMusic()
	{
		gameMusic.menu = LoadMusicStream("res/assets/audio/music/menu.mp3");
		gameMusic.gameplay = LoadMusicStream("res/assets/audio/music/gameplay.mp3");
	}

	void setMusicVolume()
	{
		SetMusicVolume(gameMusic.menu, musicVolume);
		SetMusicVolume(gameMusic.gameplay, musicVolume);
	}

	void unloadMusic()
	{
		UnloadMusicStream(gameMusic.menu);
		UnloadMusicStream(gameMusic.gameplay);
	}
}
