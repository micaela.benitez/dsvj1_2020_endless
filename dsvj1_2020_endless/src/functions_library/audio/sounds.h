#ifndef SOUNDS_H
#define SOUNDS_H

struct Sounds
{
	bool button;
	bool jump;
	bool pickup;
	bool shoot;
	bool stone_broken;
	bool loser;
};

#endif