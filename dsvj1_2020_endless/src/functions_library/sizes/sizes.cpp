#include "sizes.h"

namespace sizes
{
	int stoneWidth = 130;
	int shipWidth = 150;
	int characterWidth = 900;
	int characterWidth2 = 150;
	int characterWidth3 = 190;

	int stoneHeight = 80;
	int shipHeight = 100;
	int characterHeight = 165;

	int stone2Size = 150;

	int titleSize = 100;

	int highScoreSize = 200;

	int buttonsGameplayWidth = 100;
	int buttonsGameplayHeight = 30;

	int arrowsSize = 60;

	int coinsSize = 50;

	int textSize = 30;
	int textSize2 = 40;
	int textSize3 = 50;
	int textSize4 = 60;
	int textSize5 = 70;
	int textSize6 = 20;

	float recSize = 80;
	float recLinesSize = 5;

	float recWidth = 135;
	float rec2Width = 110;
	float recHeight = 75;

	int buttonsMenuSize = 70;

	int framesCharacter = 6;
	int framesFlag = 6;
	int framesCoin = 6;
	int framesMoon = 8;
}