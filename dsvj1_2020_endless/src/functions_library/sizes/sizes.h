#ifndef SIZES_H
#define SIZES_H

#include "scenes/gameplay/gameplay.h"

namespace sizes
{
	extern int stoneWidth;
	extern int shipWidth;
	extern int characterWidth;
	extern int characterWidth2;
	extern int characterWidth3;

	extern int stoneHeight;
	extern int shipHeight;
	extern int characterHeight;

	extern int stone2Size;

	extern int titleSize;

	extern int highScoreSize;

	extern int buttonsGameplayWidth;
	extern int buttonsGameplayHeight;

	extern int arrowsSize;

	extern int coinsSize;

	extern int textSize;
	extern int textSize2;
	extern int textSize3;
	extern int textSize4;
	extern int textSize5;
	extern int textSize6;

	extern float recSize;
	extern float recLinesSize;

	extern float recWidth;
	extern float rec2Width;
	extern float recHeight;

	extern int buttonsMenuSize;

	extern int framesCharacter;
	extern int framesFlag;
	extern int framesCoin;
	extern int framesMoon;
}

#endif