#ifndef KEYS_H
#define KEYS_H

#include "scenes/gameplay/gameplay.h"

void changeKey(int& key, int key2, int key3, int key4, int key5, int key6, int key7, int& numberKey);
void showKey(int key, int posX, int posY);
void drawKeys();

#endif