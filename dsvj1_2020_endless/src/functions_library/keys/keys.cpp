#include "keys.h"

using namespace endless;
using namespace settings;
using namespace gameplay;

void changeKey(int& key, int key2, int key3, int key4, int key5, int key6, int key7, int& numberKey)
{
    static const int firstKey = 0;
    static const int lastKey = 31;

    if (CheckCollisionPointRec(mousePoint, { pos::settingPosX2, pos::settingPosY, (float)textures::leftArrow.width, (float)textures::leftArrow.height }))
    {
        leftArrowButtonStatus = DARKGRAY;
        if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
        {
            sounds.button = true;
            numberKey--;
        }
    }
    else if (CheckCollisionPointRec(mousePoint, { pos::settingPosX3,  pos::settingPosY2, (float)textures::rightArrow.width, (float)textures::rightArrow.height }))
    {
        rightArrowButtonStatus = DARKGRAY;
        if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
        {
            sounds.button = true;
            numberKey++;
        }
    }
    else
    {
        leftArrowButtonStatus = WHITE;
        rightArrowButtonStatus = WHITE;
    }

    if ((key == key2 || key == key3 || key == key4 || key == key5 || key == key6 || key == key7) && CheckCollisionPointRec(mousePoint, { pos::settingPosX2, pos::settingPosY, (float)textures::leftArrow.width, (float)textures::leftArrow.height })) numberKey--;
    else if ((key == key2 || key == key3 || key == key4 || key == key5 || key == key6 || key == key7) && CheckCollisionPointRec(mousePoint, { pos::settingPosX3,  pos::settingPosY2, (float)textures::rightArrow.width, (float)textures::rightArrow.height })) numberKey++;

    if (numberKey < firstKey) numberKey = lastKey;
    else if (numberKey > lastKey) numberKey = firstKey;

    if (numberKey == 0) key = KEY_A;
    else if (numberKey == 1) key = KEY_B;
    else if (numberKey == 2) key = KEY_C;
    else if (numberKey == 3) key = KEY_D;
    else if (numberKey == 4) key = KEY_E;
    else if (numberKey == 5) key = KEY_F;
    else if (numberKey == 6) key = KEY_G;
    else if (numberKey == 7) key = KEY_H;
    else if (numberKey == 8) key = KEY_I;
    else if (numberKey == 9) key = KEY_J;
    else if (numberKey == 10) key = KEY_K;
    else if (numberKey == 11) key = KEY_L;
    else if (numberKey == 12) key = KEY_M;
    else if (numberKey == 13) key = KEY_N;
    else if (numberKey == 14) key = KEY_O;
    else if (numberKey == 15) key = KEY_P;
    else if (numberKey == 16) key = KEY_Q;
    else if (numberKey == 17) key = KEY_R;
    else if (numberKey == 18) key = KEY_S;
    else if (numberKey == 19) key = KEY_T;
    else if (numberKey == 20) key = KEY_U;
    else if (numberKey == 21) key = KEY_V;
    else if (numberKey == 22) key = KEY_W;
    else if (numberKey == 23) key = KEY_X;
    else if (numberKey == 24) key = KEY_Y;
    else if (numberKey == 25) key = KEY_Z;
    else if (numberKey == 26) key = KEY_UP;
    else if (numberKey == 27) key = KEY_DOWN;
    else if (numberKey == 28) key = KEY_LEFT;
    else if (numberKey == 29) key = KEY_RIGHT;
    else if (numberKey == 30) key = KEY_ENTER;
    else if (numberKey == 31) key = KEY_SPACE;
}

void showKey(int key, int posX, int posY)
{
    static const int keyPosX = 2;
    static const int keyPosX2 = 8;
    static const int keyPosX3 = 14;
    static const int keyPosY = 6;

    if (key == KEY_A) DrawText("A", posX, posY, sizes::textSize2, WHITE);
    else if (key == KEY_B) DrawText("B", posX, posY, sizes::textSize2, WHITE);
    else if (key == KEY_C) DrawText("C", posX, posY, sizes::textSize2, WHITE);
    else if (key == KEY_D) DrawText("D", posX, posY, sizes::textSize2, WHITE);
    else if (key == KEY_E) DrawText("E", posX, posY, sizes::textSize2, WHITE);
    else if (key == KEY_F) DrawText("F", posX, posY, sizes::textSize2, WHITE);
    else if (key == KEY_G) DrawText("G", posX, posY, sizes::textSize2, WHITE);
    else if (key == KEY_H) DrawText("H", posX, posY, sizes::textSize2, WHITE);
    else if (key == KEY_I) DrawText("I", posX, posY, sizes::textSize2, WHITE);
    else if (key == KEY_J) DrawText("J", posX, posY, sizes::textSize2, WHITE);
    else if (key == KEY_K) DrawText("K", posX, posY, sizes::textSize2, WHITE);
    else if (key == KEY_L) DrawText("L", posX, posY, sizes::textSize2, WHITE);
    else if (key == KEY_M) DrawText("M", posX, posY, sizes::textSize2, WHITE);
    else if (key == KEY_N) DrawText("N", posX, posY, sizes::textSize2, WHITE);
    else if (key == KEY_O) DrawText("O", posX, posY, sizes::textSize2, WHITE);
    else if (key == KEY_P) DrawText("P", posX, posY, sizes::textSize2, WHITE);
    else if (key == KEY_Q) DrawText("Q", posX, posY, sizes::textSize2, WHITE);
    else if (key == KEY_R) DrawText("R", posX, posY, sizes::textSize2, WHITE);
    else if (key == KEY_S) DrawText("S", posX, posY, sizes::textSize2, WHITE);
    else if (key == KEY_T) DrawText("T", posX, posY, sizes::textSize2, WHITE);
    else if (key == KEY_U) DrawText("U", posX, posY, sizes::textSize2, WHITE);
    else if (key == KEY_V) DrawText("V", posX, posY, sizes::textSize2, WHITE);
    else if (key == KEY_W) DrawText("W", posX, posY, sizes::textSize2, WHITE);
    else if (key == KEY_X) DrawText("X", posX, posY, sizes::textSize2, WHITE);
    else if (key == KEY_Y) DrawText("Y", posX, posY, sizes::textSize2, WHITE);
    else if (key == KEY_Z) DrawText("Z", posX, posY, sizes::textSize2, WHITE);
    else if (key == KEY_UP) DrawText("up", posX + keyPosX, posY + keyPosY, sizes::textSize6, WHITE);
    else if (key == KEY_DOWN) DrawText("down", posX - keyPosX2, posY + keyPosY, sizes::textSize6, WHITE);
    else if (key == KEY_LEFT) DrawText("left", posX - keyPosX, posY + keyPosY, sizes::textSize6, WHITE);
    else if (key == KEY_RIGHT) DrawText("right", posX - keyPosX2, posY + keyPosY, sizes::textSize6, WHITE);
    else if (key == KEY_ENTER) DrawText("enter", posX - keyPosX3, posY + keyPosY, sizes::textSize6, WHITE);
    else if (key == KEY_SPACE) DrawText("space", posX - keyPosX3, posY + keyPosY, sizes::textSize6, WHITE);
}

void drawKeys()
{
    DrawRectangleLinesEx({ pos::settingsPosX, pos::settingsPosY, sizes::recSize, sizes::recSize }, sizes::recLinesSize, upButtonStatus);
    DrawRectangleLinesEx({ pos::settingsPosX, pos::settingsPosY2, sizes::recSize, sizes::recSize }, sizes::recLinesSize, downButtonStatus);
    DrawRectangleLinesEx({ pos::settingsPosX2, pos::settingsPosY2, sizes::recSize, sizes::recSize }, sizes::recLinesSize, leftButtonStatus);
    DrawRectangleLinesEx({ pos::settingsPosX3, pos::settingsPosY2, sizes::recSize, sizes::recSize }, sizes::recLinesSize, rightButtonStatus);
    DrawRectangleLinesEx({ pos::settingsPosX, pos::settingsPosY3, sizes::recSize, sizes::recSize }, sizes::recLinesSize, bendButtonStatus);
    DrawRectangleLinesEx({ pos::settingsPosX2, pos::settingsPosY3, sizes::recSize, sizes::recSize }, sizes::recLinesSize, jumpButtonStatus);
    DrawRectangleLinesEx({ pos::settingsPosX3, pos::settingsPosY3, sizes::recSize, sizes::recSize }, sizes::recLinesSize, shootButtonStatus);

    DrawText("up", (screenWidth / 2) - MeasureText("up", sizes::textSize / 2), pos::settingsPosY4, sizes::textSize, WHITE);
    DrawText("down", (screenWidth / 2) - MeasureText("down", sizes::textSize / 2), pos::settingsPosY5, sizes::textSize, WHITE);
    DrawText("left", pos::settingsPosX4, pos::settingsPosY6, sizes::textSize, WHITE);
    DrawText("right", pos::settingsPosX3, pos::settingsPosY6, sizes::textSize, WHITE);
    DrawText("bend", (screenWidth / 2) - MeasureText("bend", sizes::textSize / 2), pos::settingsPosY7, sizes::textSize, WHITE);
    DrawText("jump", pos::settingsPosX4, pos::settingsPosY7, sizes::textSize, WHITE);
    DrawText("shoot", pos::settingsPosX3, pos::settingsPosY7, sizes::textSize, WHITE);

    showKey(key.up, (int)pos::settingsPosX5, (int)pos::settingsPosY9);
    showKey(key.down, (int)pos::settingsPosX5, (int)pos::settingsPosY10);
    showKey(key.left, (int)pos::settingsPosX6, (int)pos::settingsPosY10);
    showKey(key.right, (int)pos::settingsPosX7, (int)pos::settingsPosY10);
    showKey(key.bend, (int)pos::settingsPosX5, (int)pos::settingsPosY11);
    showKey(key.jump, (int)pos::settingsPosX6, (int)pos::settingsPosY11);
    showKey(key.shoot, (int)pos::settingsPosX7, (int)pos::settingsPosY11);
}