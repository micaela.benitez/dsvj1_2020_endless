#ifndef ENTITIES_H
#define ENTITIES_H

#include "raylib.h"

struct Entities
{
	Texture2D texture;
	float scrollingPosX;
	float posY;
	bool active;
};

#endif