#ifndef CHARACTER_H
#define CHARACTER_H

#include "raylib.h"

struct Character
{
	Vector2 position;
	float actualWay;
	float nextWay;
	float speedJump;
	bool active;
	bool jump;
	bool bend;
	bool shooter;
	bool changeOfWay;
};

#endif
