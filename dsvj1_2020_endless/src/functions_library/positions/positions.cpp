#include "positions.h"

using namespace endless;
using namespace gameplay;

namespace pos
{
	float backgroundParallaxPosX = 70.0f;
	float background2ParallaxPosX = 150.0f;
	float background3ParallaxPosX = 100.0f;
	float inicialBackgroundPosX = 0;
	float backgroundPosY = 0;

	float firstWayShip = 120;
	float middleWayShip = 385;
	float lastWayShip = 660;

	float firstWayStone = 270;
	float middleWayStone = 535;
	float lastWayStone = 810;

	float firstWayStone2 = 180;
	float middleWayStone2 = 450;
	float lastWayStone2 = 725;

	float firstWayFlag = 220;
	float middleWayFlag = 490;
	float lastWayFlag = 765;

	float firstWayCoin = 210;
	float middleWayCoin = 480;
	float lastWayCoin = 755;

	float firstWayMoon = 170;
	float middleWayMoon = 435;
	float lastWayMoon = 710;

	float firstWayCharacter = 175;
	float middleWayCharacter = 445;
	float lastWayCharacter = 720;
	float initialWayCharacterPosX = 100;
	float maxPosYJump = 150;

	float scorePosx = (screenWidth / 2) - 290;
	float scorePosX2 = (screenWidth / 2) - 180;
	float scorePosY = 10;

	float highScorePosx = (screenWidth / 2);
	float highScorePosX2 = (screenWidth / 2) + 180;
	float highScorePosY = 10;
	float menuHighScorePosX = screenWidth - 230;
	float menuHighScorePosY = (screenHeight / 2) - 100;

	float titlePoxY = 100;

	float resultTextPosX = (screenWidth / 2) - 440;
	float resultTextPosX2 = (screenWidth / 2) - 340;
	float resultTextPosX3 = (screenWidth / 2) + 80;
	float resultTextPosX4 = (screenWidth / 2) + 160;
	float resultTextPosY = (screenHeight / 2) - 250;
	float resultTextPosY2 = (screenHeight / 2) - 150;

	float menuTextPosX = screenWidth - 160;
	float menuTextPosX2 = screenWidth - 177;
	float menuTextPosX3 = screenWidth - 185;
	float menuTextPosX4 = screenWidth - 195;
	float menuTextPosX5 = screenWidth - 177;
	float menuTextPosX6 = screenWidth - 210;
	float menuTextPosX7 = screenWidth - 150;
	float menuTextPosY = (screenHeight / 2) - 55;
	float menuTextPosY2 = (screenHeight / 2) - 25;
	float menuTextPosY3 = (screenHeight / 2) + 20;
	float menuTextPosY4 = (screenHeight / 2) + 5;
	float menuTextPosY5 = (screenHeight / 2) + 35;

	float creditsTitlePosY = (screenHeight / 2) - 330;
	float creditsPosY = (screenHeight / 2) - 210;
	float creditsPosY2 = (screenHeight / 2) - 150;
	float creditsPosY3 = (screenHeight / 2) - 70;
	float creditsPosY4 = (screenHeight / 2) - 10;
	float creditsPosY5 = (screenHeight / 2) + 30;
	float creditsPosY6 = (screenHeight / 2) + 70;
	float creditsPosY7 = (screenHeight / 2) + 110;
	float creditsPosY8 = (screenHeight / 2) + 150;
	float creditsPosY9 = (screenHeight / 2) + 190;
	float creditsPosY10 = (screenHeight / 2) + 230;
	float creditsPosY11 = (screenHeight / 2) + 270;
	float credits2PosY = (screenHeight / 2) - 150;
	float credits2PosY1 = (screenHeight / 2) - 90;
	float credits2PosY2 = (screenHeight / 2) - 50;
	float credits2PosY3 = (screenHeight / 2) - 10;
	float credits2PosY4 = (screenHeight / 2) + 30;
	float credits2PosY5 = (screenHeight / 2) + 70;
	float credits2PosY6 = (screenHeight / 2) + 110;
	float credits2PosY7 = (screenHeight / 2) + 150;
	float credits2PosY8 = (screenHeight / 2) + 190;

	float instructionsTitlePosY = (screenHeight / 2) - 330;
	float instructionsPosY = (screenHeight / 2) - 190;
	float instructionsPosY2 = (screenHeight / 2) - 110;
	float instructionsPosY3 = (screenHeight / 2) - 60;
	float instructionsPosY4 = (screenHeight / 2) - 10;
	float instructionsPosY5 = (screenHeight / 2) + 90;
	float instructionsPosY6 = (screenHeight / 2) + 140;
	float instructionsPosY7 = (screenHeight / 2) + 190;
	float instructionsPosY8 = screenHeight - 110;
	float instructionsPosY9 = screenHeight - 60;
	float instructions2PosX = (screenWidth / 2) - 300;
	float instructions2PosX2 = (screenWidth / 2) - (textures::coins.width / 6) / 2;
	float instructions2PosX3 = (screenWidth / 2) + 200;
	float instructions2PosX4 = (screenWidth / 2) - 350;
	float instructions2PosX5 = (screenWidth / 2) - textures::stone.width / 2;
	float instructions2PosX6 = (screenWidth / 2) + 200;
	float instructions2PosX7 = (screenWidth / 2) - 380;
	float instructions2PosX8 = (screenWidth / 2) - 425;
	float instructions2PosX9 = (screenWidth / 2) + 180;
	float instructions2PosX10 = (screenWidth / 2) + 170;
	float instructions2PosY = (screenHeight / 2) - 210;
	float instructions2PosY2 = (screenHeight / 2) - 130;
	float instructions2PosY3 = (screenHeight / 2) - 110;
	float instructions2PosY4 = (screenHeight / 2) + 20;
	float instructions2PosY5 = (screenHeight / 2) + 140;
	float instructions2PosY6 = (screenHeight / 2) + 100;
	float instructions2PosY7 = (screenHeight / 2) + 290;
	float instructions2PosY8 = (screenHeight / 2) + 330;

	float versionPosX = 25;
	float versionPosY = screenHeight - 50;

	float settingsTitlePosY = (screenHeight / 2) - 330;
	float settingsPosX = (screenWidth / 2) - sizes::recSize / 2;
	float settingsPosX2 = (screenWidth / 2) - sizes::recSize * 2;
	float settingsPosX3 = (screenWidth / 2) + sizes::recSize;
	float settingsPosX4 = (screenWidth / 2) - sizes::recSize * 2 + 10;
	float settingsPosX5 = (screenWidth / 2) - sizes::recSize / 2 + 26;
	float settingsPosX6 = (screenWidth / 2) - sizes::recSize * 2 + 26;
	float settingsPosX7 = (screenWidth / 2) + sizes::recSize + 26;
	float settingsPosY = (screenHeight / 2) - 150;
	float settingsPosY2 = (screenHeight / 2) - 30;
	float settingsPosY3 = (screenHeight / 2) + 210;
	float settingsPosY4 = (screenHeight / 2) - 190;
	float settingsPosY5 = (screenHeight / 2) + 60;
	float settingsPosY6 = (screenHeight / 2) - 70;
	float settingsPosY7 = (screenHeight / 2) + 170;
	float settingsPosY8 = screenHeight - 50;
	float settingsPosY9 = (screenHeight / 2) - 127;
	float settingsPosY10 = (screenHeight / 2) - 7;
	float settingsPosY11 = (screenHeight / 2) + 233;

	int audioPosX = (screenWidth / 2) - 55;
	int audioPosX2 = (screenWidth / 2) - 230;
	int audioPosX3 = (screenWidth / 2) - 30;
	int audioPosX4 = (screenWidth / 2) + 145;
	int audioPosX5 = (screenWidth / 2) - 70;
	int audioPosX6 = (screenWidth / 2) - 230;
	int audioPosX7 = (screenWidth / 2) - 30;
	int audioPosX8 = (screenWidth / 2) + 145;
	int audioPosY = (screenHeight / 2) - 285;
	int audioPosY2 = (screenHeight / 2) - 120;
	int audioPosY3 = (screenHeight / 2) - 30;
	int audioPosY4 = (screenHeight / 2) + 60;
	int audioPosY5 = (screenHeight / 2) + 150;

	int resolutionsPosX = (screenWidth / 2) - 50;
	int resolutionsPosX2 = (screenWidth / 2) - 170;
	int resolutionsPosX3 = (screenWidth / 2) - 35;
	int resolutionsPosX4 = (screenWidth / 2) + 105;
	int resolutionsPosX5 = (screenWidth / 2) - 60;
	int resolutionsPosX6 = (screenWidth / 2) - 170;
	int resolutionsPosX7 = (screenWidth / 2) - 30;
	int resolutionsPosX8 = (screenWidth / 2) + 105;
	int resolutionsPosY = (screenHeight / 2) - 285;
	int resolutionsPosY2 = (screenHeight / 2) - 130;
	int resolutionsPosY3 = (screenHeight / 2) - 40;
	int resolutionsPosY4 = (screenHeight / 2) + 80;
	int resolutionsPosY5 = (screenHeight / 2) + 170;

	float tutorialPosY = (screenHeight / 2) - 330;
	float tutorialPosY2 = (screenHeight / 2) - 270;

	// Buttons
	float upButtons = 10;
	float gameplayUpButton = screenWidth - textures::exitButton2.width - 10;
	float gameplayDownButton = (screenHeight / 2) + 100;
	float gameplayCenterButton = (screenWidth / 2) - textures::audioButton.width / 2;
	float creditsUpButton = screenWidth - textures::rightArrow.width - 10;
	float credits2UpButton = screenWidth - textures::menuButton2.width - 10;

	float menuPosX = (screenWidth / 2) - (textures::startButton.width / 2);
	float menuPosX2 = screenWidth - textures::resolutionButton.width - 10;
	float menuPosY = 240;
	float menuPosY2 = 350;
	float menuPosY3 = 460;
	float menuPosY4 = 570;
	float menuPosY5 = 680;

	float resultPosX = (screenWidth / 2) - 500;
	float resultPosX2 = (screenWidth / 2) + 100;
	float resultPosX3 = (screenWidth / 2) - (textures::exitButton.width / 2);
	float resultPosY = screenHeight / 2;
	float resultPosY2 = (screenHeight / 2) + 150;

	float settingPosX = screenWidth - textures::defaultControls.width - 10;
	float settingPosX2 = 20;
	float settingPosX3 = screenWidth - textures::rightArrow.width - 20;
	float settingPosY = (screenWidth / 2) - textures::leftArrow.width / 2;
	float settingPosY2 = (screenWidth / 2) - textures::leftArrow.width / 2;

	float audioRecPosX = (screenWidth / 2) - 250;
	float audioRecPosX2 = (screenWidth / 2) - 65;
	float audioRecPosX3 = (screenWidth / 2) + 120;
	float audioRecPosX4 = (screenWidth / 2) - 250;
	float audioRecPosX5 = (screenWidth / 2) - 65;
	float audioRecPosX6 = (screenWidth / 2) + 120;
	float audioRecPosY = (screenHeight / 2) - 50;
	float audioRecPosY2 = (screenHeight / 2) + 130;

	float resolutionsRecPosX = (screenWidth / 2) - 190;
	float resolutionsRecPosX2 = (screenWidth / 2) - 50;
	float resolutionsRecPosX3 = (screenWidth / 2) + 90;
	float resolutionsRecPosX4 = (screenWidth / 2) - 190;
	float resolutionsRecPosX5 = (screenWidth / 2) - 50;
	float resolutionsRecPosX6 = (screenWidth / 2) + 90;
	float resolutionsRecPosY = (screenHeight / 2) - 60;
	float resolutionsRecPosY2 = (screenHeight / 2) + 150;
	float resolutionsRecWidth = 110;

	float quitButtonPosX = (screenWidth / 2) - textures::quitButton.width / 2;
	float quitButtonPosY = screenHeight - 100;

	// Collision positions
	int collisionAreaBendPosX = 70;
	int collisionAreaBendPosY = 40;
	int collisionAreaBendWidth = 90;
	int collisionAreaBendHeight = 40;

	int collisionAreShooter = 20;

	int collisionAreaJumpPosX = 40;
	int collisionAreaJumpPosY = 80;
	int collisionAreaJumpWidth = 110;
	int collisionAreaJumpHeight = 80;

	int collisionAreaNormalPosX = 70;
	int collisionAreaNormalWidth = 100;

	int collisionAreaShipPosY = 35;
	int collisionAreaShipHeight = 70;

	int collisionAreaStonePosX = 10;
	int collisionAreaStonePosY = 30;
	int collisionAreaStoneWidth = 20;
	int collisionAreaStoneHeight = 30;

	int collisionAreaStone2PosX = 50;
	int collisionAreaStone2Width = 90;

	void updateVariables()
	{
		backgroundParallaxPosX = 70.0f;
		background2ParallaxPosX = 150.0f;
		background3ParallaxPosX = 100.0f;
		inicialBackgroundPosX = 0;
		backgroundPosY = 0;

		if (screenHeight == 900)
		{
			firstWayShip = 120;
			middleWayShip = 385;
			lastWayShip = 660;

			firstWayStone = 270;
			middleWayStone = 535;
			lastWayStone = 810;

			firstWayStone2 = 180;
			middleWayStone2 = 450;
			lastWayStone2 = 725;

			firstWayFlag = 220;
			middleWayFlag = 490;
			lastWayFlag = 765;

			firstWayCoin = 210;
			middleWayCoin = 480;
			lastWayCoin = 755;

			firstWayMoon = 170;
			middleWayMoon = 435;
			lastWayMoon = 710;

			firstWayCharacter = 175;
			middleWayCharacter = 445;
			lastWayCharacter = 720;
		}
		else if (screenHeight == 800)
		{
			firstWayShip = 80;
			middleWayShip = 315;
			lastWayShip = 560;

			firstWayStone = 230;
			middleWayStone = 465;
			lastWayStone = 710;

			firstWayStone2 = 140;
			middleWayStone2 = 380;
			lastWayStone2 = 625;

			firstWayFlag = 180;
			middleWayFlag = 420;
			lastWayFlag = 665;

			firstWayCoin = 170;
			middleWayCoin = 410;
			lastWayCoin = 655;

			firstWayMoon = 130;
			middleWayMoon = 365;
			lastWayMoon = 610;

			firstWayCharacter = 135;
			middleWayCharacter = 375;
			lastWayCharacter = 620;
		}
		else
		{
			firstWayShip = 160;
			middleWayShip = 455;
			lastWayShip = 760;

			firstWayStone = 310;
			middleWayStone = 605;
			lastWayStone = 910;

			firstWayStone2 = 220;
			middleWayStone2 = 520;
			lastWayStone2 = 825;

			firstWayFlag = 260;
			middleWayFlag = 560;
			lastWayFlag = 865;

			firstWayCoin = 250;
			middleWayCoin = 550;
			lastWayCoin = 855;

			firstWayMoon = 210;
			middleWayMoon = 505;
			lastWayMoon = 810;

			firstWayCharacter = 215;
			middleWayCharacter = 515;
			lastWayCharacter = 820;
		}

		 initialWayCharacterPosX = 100;
		 maxPosYJump = 150;

		 scorePosx = (screenWidth / 2) - 290;
		 scorePosX2 = (screenWidth / 2) - 180;
		 scorePosY = 10;

		 highScorePosx = (screenWidth / 2);
		 highScorePosX2 = (screenWidth / 2) + 180;
		 highScorePosY = 10;
		 menuHighScorePosX = screenWidth - 230;
		 menuHighScorePosY = (screenHeight / 2) - 100;

		 titlePoxY = 100;

		 resultTextPosX = (screenWidth / 2) - 440;
		 resultTextPosX2 = (screenWidth / 2) - 340;
		 resultTextPosX3 = (screenWidth / 2) + 80;
		 resultTextPosX4 = (screenWidth / 2) + 160;
		 resultTextPosY = (screenHeight / 2) - 250;
		 resultTextPosY2 = (screenHeight / 2) - 150;

		 menuTextPosX = screenWidth - 160;
		 menuTextPosX2 = screenWidth - 177;
		 menuTextPosX3 = screenWidth - 185;
		 menuTextPosX4 = screenWidth - 195;
		 menuTextPosX5 = screenWidth - 177;
		 menuTextPosX6 = screenWidth - 210;
		 menuTextPosX7 = screenWidth - 150;
		 menuTextPosY = (screenHeight / 2) - 55;
		 menuTextPosY2 = (screenHeight / 2) - 25;
		 menuTextPosY3 = (screenHeight / 2) + 20;
		 menuTextPosY4 = (screenHeight / 2) + 5;
		 menuTextPosY5 = (screenHeight / 2) + 35;

		 creditsTitlePosY = (screenHeight / 2) - 330;
		 creditsPosY = (screenHeight / 2) - 210;
		 creditsPosY2 = (screenHeight / 2) - 150;
		 creditsPosY3 = (screenHeight / 2) - 70;
		 creditsPosY4 = (screenHeight / 2) - 10;
		 creditsPosY5 = (screenHeight / 2) + 30;
		 creditsPosY6 = (screenHeight / 2) + 70;
		 creditsPosY7 = (screenHeight / 2) + 110;
		 creditsPosY8 = (screenHeight / 2) + 150;
		 creditsPosY9 = (screenHeight / 2) + 190;
		 creditsPosY10 = (screenHeight / 2) + 230;
		 creditsPosY11 = (screenHeight / 2) + 270;
		 credits2PosY = (screenHeight / 2) - 150;
		 credits2PosY1 = (screenHeight / 2) - 90;
		 credits2PosY2 = (screenHeight / 2) - 50;
		 credits2PosY3 = (screenHeight / 2) - 10;
		 credits2PosY4 = (screenHeight / 2) + 30;
		 credits2PosY5 = (screenHeight / 2) + 70;
		 credits2PosY6 = (screenHeight / 2) + 110;
		 credits2PosY7 = (screenHeight / 2) + 150;
		 credits2PosY8 = (screenHeight / 2) + 190;

		 instructionsTitlePosY = (screenHeight / 2) - 330;
		 instructionsPosY = (screenHeight / 2) - 190;
		 instructionsPosY2 = (screenHeight / 2) - 110;
		 instructionsPosY3 = (screenHeight / 2) - 60;
		 instructionsPosY4 = (screenHeight / 2) - 10;
		 instructionsPosY5 = (screenHeight / 2) + 90;
		 instructionsPosY6 = (screenHeight / 2) + 140;
		 instructionsPosY7 = (screenHeight / 2) + 190;
		 instructionsPosY8 = screenHeight - 110;
		 instructionsPosY9 = screenHeight - 60;
		 instructions2PosX = (screenWidth / 2) - 300;
		 instructions2PosX2 = (screenWidth / 2) - (textures::coins.width / 6) / 2;
		 instructions2PosX3 = (screenWidth / 2) + 200;
		 instructions2PosX4 = (screenWidth / 2) - 350;
		 instructions2PosX5 = (screenWidth / 2) - textures::stone.width / 2;
		 instructions2PosX6 = (screenWidth / 2) + 200;
		 instructions2PosX7 = (screenWidth / 2) - 380;
		 instructions2PosX8 = (screenWidth / 2) - 425;
		 instructions2PosX9 = (screenWidth / 2) + 180;
		 instructions2PosX10 = (screenWidth / 2) + 170;
		 instructions2PosY = (screenHeight / 2) - 210;
		 instructions2PosY2 = (screenHeight / 2) - 130;
		 instructions2PosY3 = (screenHeight / 2) - 110;
		 instructions2PosY4 = (screenHeight / 2) + 20;
		 instructions2PosY5 = (screenHeight / 2) + 140;
		 instructions2PosY6 = (screenHeight / 2) + 100;
		 instructions2PosY7 = (screenHeight / 2) + 290;
		 instructions2PosY8 = (screenHeight / 2) + 330;

		 versionPosX = 25;
		 versionPosY = screenHeight - 50;

		 settingsTitlePosY = (screenHeight / 2) - 330;
		 settingsPosX = (screenWidth / 2) - sizes::recSize / 2;
		 settingsPosX2 = (screenWidth / 2) - sizes::recSize * 2;
		 settingsPosX3 = (screenWidth / 2) + sizes::recSize;
		 settingsPosX4 = (screenWidth / 2) - sizes::recSize * 2 + 10;
		 settingsPosX5 = (screenWidth / 2) - sizes::recSize / 2 + 26;
		 settingsPosX6 = (screenWidth / 2) - sizes::recSize * 2 + 26;
		 settingsPosX7 = (screenWidth / 2) + sizes::recSize + 26;
		 settingsPosY = (screenHeight / 2) - 150;
		 settingsPosY2 = (screenHeight / 2) - 30;
		 settingsPosY3 = (screenHeight / 2) + 210;
		 settingsPosY4 = (screenHeight / 2) - 190;
		 settingsPosY5 = (screenHeight / 2) + 60;
		 settingsPosY6 = (screenHeight / 2) - 70;
		 settingsPosY7 = (screenHeight / 2) + 170;
		 settingsPosY8 = screenHeight - 50;
		 settingsPosY9 = (screenHeight / 2) - 127;
		 settingsPosY10 = (screenHeight / 2) - 7;
		 settingsPosY11 = (screenHeight / 2) + 233;

		 audioPosX = (screenWidth / 2) - 55;
		 audioPosX2 = (screenWidth / 2) - 230;
		 audioPosX3 = (screenWidth / 2) - 30;
		 audioPosX4 = (screenWidth / 2) + 145;
		 audioPosX5 = (screenWidth / 2) - 70;
		 audioPosX6 = (screenWidth / 2) - 230;
		 audioPosX7 = (screenWidth / 2) - 30;
		 audioPosX8 = (screenWidth / 2) + 145;
		 audioPosY = (screenHeight / 2) - 285;
		 audioPosY2 = (screenHeight / 2) - 120;
		 audioPosY3 = (screenHeight / 2) - 30;
		 audioPosY4 = (screenHeight / 2) + 60;
		 audioPosY5 = (screenHeight / 2) + 150;

		 resolutionsPosX = (screenWidth / 2) - 50;
		 resolutionsPosX2 = (screenWidth / 2) - 170;
		 resolutionsPosX3 = (screenWidth / 2) - 35;
		 resolutionsPosX4 = (screenWidth / 2) + 105;
		 resolutionsPosX5 = (screenWidth / 2) - 60;
		 resolutionsPosX6 = (screenWidth / 2) - 170;
		 resolutionsPosX7 = (screenWidth / 2) - 30;
		 resolutionsPosX8 = (screenWidth / 2) + 105;
		 resolutionsPosY = (screenHeight / 2) - 285;
		 resolutionsPosY2 = (screenHeight / 2) - 130;
		 resolutionsPosY3 = (screenHeight / 2) - 40;
		 resolutionsPosY4 = (screenHeight / 2) + 80;
		 resolutionsPosY5 = (screenHeight / 2) + 170;

		 tutorialPosY = (screenHeight / 2) - 330;
		 tutorialPosY2 = (screenHeight / 2) - 270;

		// Buttons
		 upButtons = 10;
		 gameplayUpButton = screenWidth - textures::exitButton2.width - 10;
		 gameplayDownButton = (screenHeight / 2) + 100;
		 gameplayCenterButton = (screenWidth / 2) - textures::audioButton.width / 2;
		 creditsUpButton = screenWidth - textures::rightArrow.width - 10;
		 credits2UpButton = screenWidth - textures::menuButton2.width - 10;

		 menuPosX = (screenWidth / 2) - (textures::startButton.width / 2);
		 menuPosX2 = screenWidth - textures::resolutionButton.width - 10;
		 menuPosY = 240;
		 menuPosY2 = 350;
		 menuPosY3 = 460;
		 menuPosY4 = 570;
		 menuPosY5 = 680;

		 resultPosX = (screenWidth / 2) - 450;
		 resultPosX2 = (screenWidth / 2) + 40;
		 resultPosX3 = (screenWidth / 2) - (textures::exitButton.width / 2);
		 resultPosY = screenHeight / 2;
		 resultPosY2 = (screenHeight / 2) + 150;

		 settingPosX = screenWidth - textures::defaultControls.width - 10;
		 settingPosX2 = 20;
		 settingPosX3 = screenWidth - textures::rightArrow.width - 20;
		 settingPosY = (screenWidth / 2) - textures::leftArrow.width / 2;
		 settingPosY2 = (screenWidth / 2) - textures::leftArrow.width / 2;

		 audioRecPosX = (screenWidth / 2) - 250;
		 audioRecPosX2 = (screenWidth / 2) - 65;
		 audioRecPosX3 = (screenWidth / 2) + 120;
		 audioRecPosX4 = (screenWidth / 2) - 250;
		 audioRecPosX5 = (screenWidth / 2) - 65;
		 audioRecPosX6 = (screenWidth / 2) + 120;
		 audioRecPosY = (screenHeight / 2) - 50;
		 audioRecPosY2 = (screenHeight / 2) + 130;

		 resolutionsRecPosX = (screenWidth / 2) - 190;
		 resolutionsRecPosX2 = (screenWidth / 2) - 50;
		 resolutionsRecPosX3 = (screenWidth / 2) + 90;
		 resolutionsRecPosX4 = (screenWidth / 2) - 190;
		 resolutionsRecPosX5 = (screenWidth / 2) - 50;
		 resolutionsRecPosX6 = (screenWidth / 2) + 90;
		 resolutionsRecPosY = (screenHeight / 2) - 60;
		 resolutionsRecPosY2 = (screenHeight / 2) + 150;
		 resolutionsRecWidth = 110;

		 quitButtonPosX = (screenWidth / 2) - textures::quitButton.width / 2;
		 quitButtonPosY = screenHeight - 100;

		 // Collision positions
		 collisionAreaBendPosX = 70;
		 collisionAreaBendPosY = 40;
		 collisionAreaBendWidth = 90;
		 collisionAreaBendHeight = 40;

		 collisionAreShooter = 20;

		 collisionAreaJumpPosX = 40;
		 collisionAreaJumpPosY = 80;
		 collisionAreaJumpWidth = 110;
		 collisionAreaJumpHeight = 80;

		 collisionAreaNormalPosX = 70;
		 collisionAreaNormalWidth = 100;

		 collisionAreaShipPosY = 35;
		 collisionAreaShipHeight = 70;

		 collisionAreaStonePosX = 10;
		 collisionAreaStonePosY = 30;
		 collisionAreaStoneWidth = 20;
		 collisionAreaStoneHeight = 30;

		 collisionAreaStone2PosX = 50;
		 collisionAreaStone2Width = 90;
	}
}