#ifndef POSITIONS_H
#define POSITIONS_H

#include "scenes/gameplay/gameplay.h"
#include "functions_library/sizes/sizes.h"

namespace pos
{
	extern float backgroundParallaxPosX;
	extern float background2ParallaxPosX;
	extern float background3ParallaxPosX;
	extern float inicialBackgroundPosX;
	extern float backgroundPosY;

	extern float firstWayShip;
	extern float middleWayShip;
	extern float lastWayShip;

	extern float firstWayStone;
	extern float middleWayStone;
	extern float lastWayStone;

	extern float firstWayStone2;
	extern float middleWayStone2;
	extern float lastWayStone2;
	
	extern float firstWayFlag;
	extern float middleWayFlag;
	extern float lastWayFlag;

	extern float firstWayCoin;
	extern float middleWayCoin;
	extern float lastWayCoin;

	extern float firstWayMoon;
	extern float middleWayMoon;
	extern float lastWayMoon;

	extern float firstWayCharacter;
	extern float middleWayCharacter;
	extern float lastWayCharacter;
	extern float initialWayCharacterPosX;
	extern float maxPosYJump;

	extern float scorePosx;
	extern float scorePosX2;
	extern float scorePosY;

	extern float highScorePosx;
	extern float highScorePosX2;
	extern float highScorePosY;
	extern float menuHighScorePosX;
	extern float menuHighScorePosY;

	extern float titlePoxY;

	extern float resultTextPosX;
	extern float resultTextPosX2;
	extern float resultTextPosX3;
	extern float resultTextPosX4;
	extern float resultTextPosY;
	extern float resultTextPosY2;

	extern float menuTextPosX;
	extern float menuTextPosX2;
	extern float menuTextPosX3;
	extern float menuTextPosX4;
	extern float menuTextPosX5;
	extern float menuTextPosX6;
	extern float menuTextPosX7;
	extern float menuTextPosY;
	extern float menuTextPosY2;
	extern float menuTextPosY3;
	extern float menuTextPosY4;
	extern float menuTextPosY5;

	extern float creditsTitlePosY;
	extern float creditsPosY;
	extern float creditsPosY2;
	extern float creditsPosY3;
	extern float creditsPosY4;
	extern float creditsPosY5;
	extern float creditsPosY6;
	extern float creditsPosY7;
	extern float creditsPosY8;
	extern float creditsPosY9;
	extern float creditsPosY10;
	extern float creditsPosY11;
	extern float credits2PosY;
	extern float credits2PosY1;
	extern float credits2PosY2;
	extern float credits2PosY3;
	extern float credits2PosY4;
	extern float credits2PosY5;
	extern float credits2PosY6;
	extern float credits2PosY7;
	extern float credits2PosY8;

	extern float instructionsTitlePosY;
	extern float instructionsPosY;
	extern float instructionsPosY2;
	extern float instructionsPosY3;
	extern float instructionsPosY4;
	extern float instructionsPosY5;
	extern float instructionsPosY6;
	extern float instructionsPosY7;
	extern float instructionsPosY8;
	extern float instructionsPosY9;
	extern float instructions2PosX;
	extern float instructions2PosX2;
	extern float instructions2PosX3;
	extern float instructions2PosX4;
	extern float instructions2PosX5;
	extern float instructions2PosX6;
	extern float instructions2PosX7;
	extern float instructions2PosX8;
	extern float instructions2PosX9;
	extern float instructions2PosX10;
	extern float instructions2PosY;
	extern float instructions2PosY2;
	extern float instructions2PosY3;
	extern float instructions2PosY4;
	extern float instructions2PosY5;
	extern float instructions2PosY6;
	extern float instructions2PosY7;
	extern float instructions2PosY8;

	extern float versionPosX;
	extern float versionPosY;

	extern float settingsTitlePosY;
	extern float settingsPosX;
	extern float settingsPosX2;
	extern float settingsPosX3;
	extern float settingsPosX4;
	extern float settingsPosX5;
	extern float settingsPosX6;
	extern float settingsPosX7;
	extern float settingsPosY;
	extern float settingsPosY2;
	extern float settingsPosY3;
	extern float settingsPosY4;
	extern float settingsPosY5;
	extern float settingsPosY6;
	extern float settingsPosY7;
	extern float settingsPosY8;
	extern float settingsPosY9;
	extern float settingsPosY10;
	extern float settingsPosY11;

	extern int audioPosX;
	extern int audioPosX2;
	extern int audioPosX3;
	extern int audioPosX4;
	extern int audioPosX5;
	extern int audioPosX6;
	extern int audioPosX7;
	extern int audioPosX8;
	extern int audioPosY;
	extern int audioPosY2;
	extern int audioPosY3;
	extern int audioPosY4;
	extern int audioPosY5;

	extern int resolutionsPosX;
	extern int resolutionsPosX2;
	extern int resolutionsPosX3;
	extern int resolutionsPosX4;
	extern int resolutionsPosX5;
	extern int resolutionsPosX6;
	extern int resolutionsPosX7;
	extern int resolutionsPosX8;
	extern int resolutionsPosY;
	extern int resolutionsPosY2;
	extern int resolutionsPosY3;
	extern int resolutionsPosY4;
	extern int resolutionsPosY5;

	extern float tutorialPosY;
	extern float tutorialPosY2;

	// Buttons
	extern float upButtons;
	extern float gameplayUpButton;
	extern float gameplayDownButton;
	extern float gameplayCenterButton;
	extern float creditsUpButton;
	extern float credits2UpButton;

	extern float menuPosX;
	extern float menuPosX2;
	extern float menuPosY;
	extern float menuPosY2;
	extern float menuPosY3;
	extern float menuPosY4;
	extern float menuPosY5;

	extern float resultPosX;
	extern float resultPosX2;
	extern float resultPosX3;
	extern float resultPosY;
	extern float resultPosY2;

	extern float settingPosX;
	extern float settingPosX2; 
	extern float settingPosX3;
	extern float settingPosY;
	extern float settingPosY2;

	extern float audioRecPosX;
	extern float audioRecPosX2;
	extern float audioRecPosX3;
	extern float audioRecPosX4;
	extern float audioRecPosX5;
	extern float audioRecPosX6;
	extern float audioRecPosY;
	extern float audioRecPosY2;

	extern float resolutionsRecPosX;
	extern float resolutionsRecPosX2;
	extern float resolutionsRecPosX3;
	extern float resolutionsRecPosX4;
	extern float resolutionsRecPosX5;
	extern float resolutionsRecPosX6;
	extern float resolutionsRecPosY;
	extern float resolutionsRecPosY2;
	extern float resolutionsRecWidth;

	extern float quitButtonPosX;
	extern float quitButtonPosY;

	extern void updateVariables();

	// Collision positions
	extern int collisionAreaBendPosX;
	extern int collisionAreaBendPosY;
	extern int collisionAreaBendWidth;
	extern int collisionAreaBendHeight;

	extern int collisionAreShooter;

	extern int collisionAreaJumpPosX;
	extern int collisionAreaJumpPosY;
	extern int collisionAreaJumpWidth;
	extern int collisionAreaJumpHeight;

	extern int collisionAreaNormalPosX;
	extern int collisionAreaNormalWidth;

	extern int collisionAreaShipPosY;
	extern int collisionAreaShipHeight;

	extern int collisionAreaStonePosX;
	extern int collisionAreaStonePosY;
	extern int collisionAreaStoneWidth;
	extern int collisionAreaStoneHeight;

	extern int collisionAreaStone2PosX;
	extern int collisionAreaStone2Width;
}

#endif